

jQuery(document).ready(function( $ ) {

    var boxit = $('#boxit');
	if(boxit.length == 0) return;

    //user agent detection - drop zone must be disabled on touch devices
    var device = boxit_ua_detect({ useUA: true });
    var dropzone = (device == 'desktop')? "boxit_dropzone" : "";

	/*
     * Drop support for IE <= 9
     */
    var ua = navigator.userAgent;
    if(ua.indexOf("MSIE 9") != -1 || ua.indexOf("MSIE 8") != -1 || ua.indexOf("MSIE 7") != -1) {
        $('#boxit_pre_upload').html('<p>'+__trans.not_supported+'</p>');
	    return;
    }


    boxit_space_status();



    /***************************************************/
    /*                                                 */
    /*       PLUPLOAD EVENT SETUP                      */
    /*                                                 */
    /***************************************************/
    var filetypes = (boxit.attr('data-file_types'))? [{ title : "File types", extensions : boxit.attr('data-file_types') }] : [];
    var uploader = new plupload.Uploader({
        browse_button: 'boxit_browse', // this can be an id of a DOM element or the DOM element itself
        //path to php upload file
        url: boxit.attr('data-upload_script'),
        filters: {
            //if no mime_types are passed all filetypes are allowed
            mime_types : filetypes,
            max_file_size : boxit.attr('data-size_limit'),
            prevent_duplicates : true
        },
        // split files in chunks (spacified in kb)
        chunk_size : boxit.attr('data-chunk'), //split files to specified chunks
        drop_element : dropzone, //"boxit_dropzone", //drop element ID
        container : "boxit" // the container of the whole application (ID)
    });
    uploader.init();

    /**
     * plupload: add file to the queue
     */
    uploader.bind('FilesAdded', function(up, files) {
        var html = '';

        $('.boxit_start_upload_container, #queue_header').show();

        plupload.each(files, function(file) {
            //safe filenme
            file.name = file.name.replace(/[^a-z0-9-_\.\s]+/gi,"").toLowerCase();;
            //shoren filename if needed
            var filename = $.trim(file.name);
            if (filename.length > 30) {
                filename = filename.substring(0, 15)+'...'+filename.substring(filename.length -15);
            }
            html += '<li class="boxit_bgd_2 boxit_text_1" id="' + file.id + '"><span>' + filename + ' (' + plupload.formatSize(file.size) + ')</span><b></b></li>';
        });
        document.getElementById('filelist').innerHTML += html;
    });

    /**
     * plupload: show upload progress
     */
    uploader.bind('UploadProgress', function(up, file) {
        document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span class="boxit_bgd_1" style="width:'+file.percent+'%"></span>';
    });

    /**
     * plupload: show error messages
     */
    uploader.bind('Error', function(up, err) {
        var debug = $('#boxit').attr('data-debug');
        if(debug) console.log('PLU Error '+err.code+' (file: '+err.file.name+'): '+err.message);
    });

    /**
     * plupload: start the upload queue when "start upload" button is clicked
     */
    document.getElementById('boxit_start_upload').onclick = function() {
        //each time new upload queue is being triggered - trigger the data-files attr
        $('#boxit').removeAttr('data-files');
        uploader.start();
        if($('#boxit').attr('data-hide_on_start') == '1') {
            $('#boxit_start_upload').slideUp(200);
            $('#boxit_dropzone').slideUp(200, function() {
                $(this).remove();
            });
        }
    };

    /**
     * plupload: after each file is uploaded send it to Dropbox trough move_to_dropbox() ajax function
     */
    uploader.bind('FileUploaded', function(up, file, response) {
        var obj = jQuery.parseJSON(response.response);
        var debug = $('#boxit').attr('data-debug');

        if(obj.result != undefined) {
            $('#'+file.id).addClass('boxit_file_uploaded');
            $('#'+file.id).addClass('boxit_syncing');
            //move the file to dropbox with TRUE indicating upload success
            if(debug) console.log('PLU "FileUploaded" callback: (file '+file.name+') -> OK');
            move_to_dropbox(file, true);
            if($('#queue_header div').length == 0) $('#queue_header').append('<div> -<span class="moving_to_dbx boxit_col_2">l</span>'+__trans.syncing_with_dbx+'</div>');
        } else {
            //call the ajax function fith FALSE indicating upload error
            if(debug) {
                console.log('PLU "FileUploaded" callback: (file '+file.name+') -> ERROR:');
                console.log(response);
            }
            move_to_dropbox(file, false);
        }
    });

    /**
     * plupload: when all files from the queue have been uploaded/processed add data-files attr to #boxit - it will be cought by move_to_dropbox() ajax function on last file
     */
    uploader.bind('UploadComplete', function(up, files) {
        $('#boxit').attr('data-files', JSON.stringify(files));
    });






    /***************************************************/
    /*                                                 */
    /*       NAME/EMAIL & UPLOAD MESSAGE setup         */
    /*                                                 */
    /***************************************************/
    function boxit_focus_blur(el) {
        var el = $(el);
        el.focus(function(){
            $(this).removeClass('boxit_border_2').addClass('boxit_border_1');
        });
        el.blur(function(){
            var t = $(this);
            t.removeClass('boxit_border_1').addClass('boxit_border_2');
            t.val(t.val().replace(/"/g, ''));
            t.val(t.val().replace(/\\/g, ''));
            if(!el.hasClass('boxit_upload_message')) {
                //remove spaces
                t.val(t.val().replace(/\s/g, ''));
            }
            //if($(this).val().split("/").length > 0)
            while(t.val().charAt(0) === '/') {
                t.val(t.val().substr(1));
            }
                //console.log($(this).val().charAt($(this).val().length-1));
        });
    }

    if($('#boxit_pre_upload').length > 0) {

        /*ADDED*/
        if(device == 'smartphone') {
            $('#boxit').slideDown(200);
            $('#boxit').slideUp(200);
        }

        boxit_focus_blur('.boxit_name');
        boxit_focus_blur('.boxit_email');
        boxit_focus_blur('.boxit_upload_message');

        var show_boxit_button = $('#show_boxit'),
            name_email = $('#name_email'),
            upload_message = $('#upload_message'),
            val_error = false;

        //If captcha enabled
        var captcha_form = $('#boxit_captcha');
        if(captcha_form.length > 0) {
            $('#boxit_captcha_text').realperson();
            captcha_form.find('.realperson-text').addClass('boxit_col_1');
        }

        show_boxit_button.on('click', function(e) {
            val_error = false;

            //check the name & email fields are in use
            if(name_email.is(':visible')) {
                //remove errors if exist
                name_email.find('.pre_error_container').remove();

                var name_field = name_email.find('.boxit_name'),
                    email_field = name_email.find('.boxit_email'),
                    regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                if(name_field.val() == '') {
                    name_field.parent().append('<div class="pre_error_container"><span class="boxit_pre_error">'+__trans.name_req+'</span></div>');
                    val_error = true;
                }
                if(email_field.val() == '') {
                    email_field.parent().append('<div class="pre_error_container"><span class="boxit_pre_error">'+__trans.email_req+'</span></div>');
                    val_error = true;
                }
                if(email_field.val() != '' && !regex.test(email_field.val())) {
                    email_field.parent().append('<div class="pre_error_container"><span class="boxit_pre_error">'+__trans.wrong_email+'</span></div>');
                    val_error = true;
                }
            }

            //If captcha enabled - validate
            if(captcha_form.length > 0) {
                //remove captcha error messages if there are any
                captcha_form.find('.pre_error_container').remove();
                //submit the captcha form and gather captcha value  + user input
                captcha_form.submit();
            } else {
                if(!val_error) {
                    $('#boxit_pre_upload').slideUp(300);
                    $('#boxit').slideDown(300);
                }
            }
        });

        captcha_form.on('submit', function() {
            var c_form = $(this),
                captcha = c_form.find('input[name="captcha"]').val(),
                captchaHash = c_form.find('input[name="captchaHash"]').val();
            //chack captcha value using ajax call
            $.ajax({
                type: 'POST',
                url: boxit_ajax.ajaxurl,
                data: {
                    action: 'boxit_check_captcha',
                    captcha: captcha,
                    captchaHash: captchaHash
                },
                success: function(data, textStatus, jqXHR) {
                    //c_form.append(data);
                    if(data !== 'ok') {
                        $('#boxit_captcha_text').parent().append('<div class="pre_error_container"><span class="boxit_pre_error">'+__trans.wrong_captcha+'</span></div>');
                    } else {
                        if(!val_error) {
                            $('#boxit_pre_upload').slideUp(300);
                            $('#boxit').slideDown(300);
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                }
            });
            return false; //don't submit the captcha form
        });
    }












    /***************************************************/
    /*                                                 */
    /*       Dropzone OVER /OUT /DROP detection        */
    /*                                                 */
    /***************************************************/
    var inner = false;
    $("#boxit_dropzone").on('dragenter', function (e) {
        $(this).addClass('drop_area_active');
    });
    $("#boxit_dropzone div, #boxit_dropzone span").on('dragenter', function (e) {
        inner = true;
    });
    $("#boxit_dropzone div, #boxit_dropzone span").on('dragleave', function (e) {
        inner = false;
    });
    $("#boxit_dropzone").on('dragleave', function (e) {
        var target = event.target || event.srcElement;
        target = $(target);
        if(!inner && target.hasClass('drop_area_active')) {
            $("#boxit_dropzone").removeClass('drop_area_active');
        }
    });
    $("#boxit_dropzone, #boxit_dropzone div, #boxit_dropzone span").on('drop', function (e) {
        $("#boxit_dropzone").removeClass('drop_area_active');
        inner = false;
    });


});







/**
 * Device detection taken from Miguel Fonseca Github Gist.
 * Source: https://gist.github.com/mcsf/3803021
 * Author Miguel Fonseca (https://gist.github.com/mcsf)
 */
var boxit_ua_detect = function(options) {
    if (options === undefined) options = {};
    this.init = function() {
        this.mm = window.matchMedia;

        if (this.mm && !options.useUA) {
            this.method = 'media queries';
            this.type   = mq();
        }
        else {
            this.method = 'user agent strings';
            this.type   = ua();
        }
        return options.verbose ? [ this.type, this.method ] : this.type;
    };
    /**
     * Media queries
     */
    var mq = function() {
        if (minMatch(320) && maxMatch(480))
            return 'smartphone';

        if (minMatch(768) && maxMatch(1024))
            return 'tablet';

        return 'desktop';
    };
    var match = function(m, width) {
        return window.matchMedia(
                'screen and (' + m + '-device-width: '
                    + width + 'px)').matches;
    };
    var minMatch = function(width) {
        return match('min', width);
    };
    var maxMatch = function(width) {
        return match('max', width);
    };
    /**
     * User agent strings
     */
    var ua = function() {
        var uas = navigator.userAgent;
        tablets = [ /Android 3/i, /iPad/i ];
        for (i=0, l=tablets.length; i<l; i++)
            if (uas.match(tablets[i])) return 'tablet';
        smartphones = [
            /Mobile/i,
            /Android/i,
            /iPhone/i,
            /BlackBerry/i,
            /Windows Phone/i,
            /Windows Mobile/i,
            /Maemo/i,
            /PalmSource/i,
            /SymbianOS/i,
            /SymbOS/i,
            /Nokia/i,
            /MOT-/i,
            /JDME/i,
            /Series 60/i,
            /S60/i,
            /SonyEricsson/i,
        ];
        for (i=0, l=smartphones.length; i<l; i++)
            if (uas.match(smartphones[i])) return 'smartphone';
        return 'desktop';
    };
    return this.init();
};



function boxit_space_status() {
    jQuery(document).ready(function( $ ) {
        if($('#boxit').attr('data-hide_space') != 0) return;
        var holder = $('.space_status');
        holder.html('<span class="dbx_spin">l</span>');
        $.ajax({
            type: 'POST',
            url: boxit_ajax.ajaxurl,
            data: {
                action: 'boxit_space_status'
            },
            success: function(data, textStatus, jqXHR) {
                holder.html(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
            }
        });
    });
}
