$(".pagination-cards").each(function(index, pagination) {

    var pagination_max_page_number = parseInt($(pagination).attr('data-pagination-max-page-number'));
    var pagination_page_id = $(pagination).attr('data-pagination-page-id');
    var nextButton = $(pagination).find('.pagination-next');
    var previousButton = $(pagination).find('.pagination-previous');

    nextButton.click(function(event) {
        event.preventDefault();
        var pagination_page_number = $(pagination).attr('data-pagination-page-number');
        var currentPageNumber = parseInt(pagination_page_number);
        var pageNumber = currentPageNumber + 1;

        if (!(pageNumber > pagination_max_page_number)) {
            paginate(pageNumber);
        }
    });

    var paginate = function(pageNumber){

        var search_term = null;
        if (pagination_page_id == 'discover-previews-section') {
            search_term = $('input[name="search-term"]').val();
        }

        $.ajax({
            type: 'GET',
            url: "/ajax/pagination-update/",
            data: {
                'page_number': pageNumber,
                'pagination_page_id': pagination_page_id,
                'search_term': search_term
            },
            success: function (data) {
                $('#'+pagination_page_id).html(data);
                setup_profile_previews_layouts();
                setup_pledges_ui();
                setup_auto_font_resize();
                $(pagination).attr('data-pagination-page-number', pageNumber);
                $('#'+pagination_page_id).goTo();

                var previous = $(pagination).find('.previous');
                var next = $(pagination).find('.next');

                if (pageNumber > 1) {
                    previous.removeClass('disabled');
                }

                if (pageNumber == 1) {
                    previous.addClass('disabled');
                }

                if (pageNumber == pagination_max_page_number) {
                    next.addClass('disabled');
                }

                if (pageNumber < pagination_max_page_number) {
                    next.removeClass('disabled');
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    };

    previousButton.click(function(event){
        event.preventDefault();
        var pagination_page_number = $(pagination).attr('data-pagination-page-number');
        var currentPageNumber = parseInt(pagination_page_number);
        var pageNumber = currentPageNumber - 1;
        if (!(pageNumber <= 0)) {
            paginate(pageNumber);
        }
    });

});


(function($) {
    $.fn.goTo = function() {
        $('html, body').animate({
            scrollTop: (parseInt($(this).offset().top)-100) + 'px'
        }, 'fast');
        return this; // for chaining...
    };
})(jQuery);
