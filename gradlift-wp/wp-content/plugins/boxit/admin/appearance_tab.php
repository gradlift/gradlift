<?php



function appearance_tab_fields() {
    $options = get_option('boxit_options');
    $fields = array(
        /* section */
        array(
            'id' => 'appearance',
            'title' => 'BOXIT appearance',
            'fields' => array(
                /* fields */
                array(
                    'id' => 'primary_color',
                    'title' => 'Primary color:'
                ),
                array(
                    'id' => 'secondary_color',
                    'title' => 'Secondary color:'
                ),
                array(
                    'id' => 'text_on_primary',
                    'title' => 'Text on primary & secondary color:'
                ),
                array(
                    'id' => 'hide_logo',
                    'title' => 'Hide BOXIT logo?'
                ),
                array(
                    'id' => 'hide_space',
                    'title' => 'Hide Dropbox disc space indicator'
                )
            )
        )
    );
    return $fields;
}






function boxit_appearance_section() {
    echo '<input type="hidden" name="boxit_tab" value="appearance"/>';
}

function boxit_appearance_primary_color() {
    $options = get_option('boxit_options');
    ?>
    <input maxlength="7" size="5" type="text" name="boxit_options[primary_color]" id="boxit_primary_color_color_picker"  value="<?php echo $options['primary_color']; ?>" />
    <?php
}

function boxit_appearance_secondary_color() {
    $options = get_option('boxit_options');
    ?>
    <input maxlength="7" size="5" type="text" name="boxit_options[secondary_color]" id="boxit_secondary_color_color_picker"  value="<?php echo $options['secondary_color']; ?>" />
    <?php
}

function boxit_appearance_text_on_primary() {
    $options = get_option('boxit_options');
    ?>
    <input maxlength="7" size="5" type="text" name="boxit_options[text_on_primary]" id="boxit_text_on_primary_color_picker"  value="<?php echo $options['text_on_primary']; ?>" />
    <?php
}

function boxit_appearance_hide_logo() {
    $options = get_option('boxit_options');
    ?>
    <select name="boxit_options[hide_logo]" id="hide_logo">
        <option <?php if($options['hide_logo'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['hide_logo'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <?php
}

function boxit_appearance_hide_space() {
    $options = get_option('boxit_options');
    if(!isset($options['hide_space'])) $options['hide_space'] = '0';
    ?>
    <select name="boxit_options[hide_space]" id="hide_space">
        <option <?php if($options['hide_space'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['hide_space'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <?php
}



