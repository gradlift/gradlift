USER SUGGESTIONS:

 - load scripts only when shortcake is present (http://scribu.net/wordpress/optimal-script-loading.html - jedi master way)

- possibility to remove items from the queue

—————————————————————————————
BOXIT v 2.7

- fixes major security issue.

BOXIT v 2.6.2

- fixes conflict with “hide my WP plugin”

BOXIT v 2.6:

1. Dropbox SDK updated to v 1.1.5

BOXIT v 2.5.9:

1. Added option eanbling "thank you message" after all filels from queue are succesfully uploaded.

2. Added option anabling redirection to custom "thank you page" after all filels from queue are succesfully uploaded.

3. Added option to hide "Start upload" button & "drop zone" when upload starts.

BOXIT v 2.5.7:

1. fixed bug "unable to select files in safari on IOS devices"

2. added confirmation message after all files finish uploading (under dropzone)

BOXIT v 2.5.6.1:

1. minor bug fixes

BOXIT v 2.5.6:

1. fixed: removing spaces from comment field.

2. Added: Dropbox disc usage status indication (can be disabled in Appearance tab).

3. Fixed: "'inStream' has bad type; expecting resource, got boolean" error caused by filenames with invalid characters

BOXIT v 2.5.2:

1. Fixed Dropbox API error

2. Added CAPTCHA

BOXIT v 2.5.1:

1. You can add {date} to boxit shortcode "custom_folder" param. - The {date} will be converted to current date in day-month-year format (12-08-2014) ex: [boxit custom_folder="images/{date}"] will upload files to "Dropbox/Applications/your_app_name/images/12-08-2014"

2. Added option to organize uploads into day, month, year based folders (see BOXIT general settings for more info)

3. Fixed issue that forced user to "install BOXIT" after every plugin update.

4. fixes conflict with BackupBuddy plugin

5. Fixed illegal "\" characters in name / email fields

BOXIT v 2.5:

1. New file upload handeling - now BOXIT uses Plupload to handle file upload to the server (befor it gets transfer to Dropbox)

2. Removed option: "Prevent overwriting files?" - Dropbox makes sure files with the same name won't be overwritten.

3. Added optional debug console (in developer toolbar) with more detailed upload status tracking and error reporting (can be enabled/disabeld in BOXIT general settings)

4. Email notification is send out autamaticly when all files are uploaded. no need to click the "done" button.

5. Added translation tab in BOXIT settings and ability to translate BOXIT front end & email noticication file statuses

6. Added {folder} tag to email notification template

7. Added Dropbox public file links to upload notification email (in filelist)

8. Upload message field label text moved from general tab to translation tab (boxit settings)

9. "no support for 64-bit integers" error message removed and replaced with the warning in BOXIT settings

10. Added detection for PHP version - preventing displing the  "namespace support" error in PHP < 5.3

11. The name field value (from "name & email" fields) can now be used as upload directory if setting "If 'name/email' fields are enabled store files in folder from 'name' input?" is set to YES. For example if user types "my_company/procject name" inside the "name" field files will be uploaded to "Dropbox/Applications/your_app_name/my_company/procject name".

