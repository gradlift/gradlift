# coding: utf-8
import os
import datetime

from django import forms
from django.contrib import admin

from ckeditor.widgets import CKEditorWidget

from gradlift.apps import Utility
from gradlift.apps.data import models


class RefUserTypeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['name', 'code', 'description']})
    ]

    list_display = ('code', 'name', 'description', )
    search_fields = ['name', 'code']


admin.site.register(models.RefUserType, RefUserTypeAdmin)


class RefCountryAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['name', 'code']})
    ]

    list_display = ('code', 'name', )
    search_fields = ['name', 'code']


admin.site.register(models.RefCountry, RefCountryAdmin)


class RefStateAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['name', 'code']})
    ]

    list_display = ('code', 'name', )
    search_fields = ['name', 'code']


admin.site.register(models.RefState, RefStateAdmin)


class RefSeasonAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['name', 'code', 'order']})
    ]

    list_display = ('code', 'name', )
    search_fields = ['name', 'code']


admin.site.register(models.RefSeason, RefSeasonAdmin)


class InstitutionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['name', 'unit_id']}),
        ('Address',             {'fields': ['street', 'city', 'state', 'zip_code', 'country']}),
        ('Contacts',            {'fields': ['phone_number', 'fax_number', 'website']})
    ]

    list_display = ('name', 'unit_id', )
    search_fields = ['name', 'unit_id']


admin.site.register(models.Institution, InstitutionAdmin)


class RefDegreeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['name', 'code', 'order']})
    ]

    list_display = ('code', 'name', )
    search_fields = ['name', 'code']


admin.site.register(models.RefDegree, RefDegreeAdmin)


class RefMajorAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['name', 'code']})
    ]

    list_display = ('code', 'name', )
    search_fields = ['name', 'code']


admin.site.register(models.RefMajor, RefMajorAdmin)


class RefTierAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['name', 'code', 'description', 'integer_value']})
    ]

    list_display = ('code', 'name', 'description', 'integer_value',)
    search_fields = ['name', 'code', 'integer_value']


admin.site.register(models.RefTier, RefTierAdmin)


class RefOrganizationTypeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['name', 'code', 'description']})
    ]

    list_display = ('code', 'name', 'description', )
    search_fields = ['name', 'code']


admin.site.register(models.RefOrganizationType, RefOrganizationTypeAdmin)


class RefContributionTypeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['order', 'name', 'code', 'description']})
    ]

    list_display = ('code', 'name', 'description', )
    search_fields = ['name', 'code']


admin.site.register(models.RefContributionType, RefContributionTypeAdmin)


class RefSocialNetworkTypeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['name', 'code', 'order']})
    ]

    list_display = ('code', 'name', )
    search_fields = ['name', 'code']


admin.site.register(models.RefSocialNetworkType, RefSocialNetworkTypeAdmin)


class RefNotificationTypeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['name', 'code', 'description']})
    ]

    list_display = ('code', 'name', )
    search_fields = ['name', 'code']


admin.site.register(models.RefNotificationType, RefNotificationTypeAdmin)


class OrganizationAgreementAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['description', 'creation_date', 'expiration_date', 'is_active', 'tier']})
    ]

    list_display = ('description', 'creation_date', 'expiration_date', 'is_active', 'tier',)
    search_fields = ['description', 'creation_date', 'expiration_date', 'is_active', 'tier']
    list_filter = ['is_active', 'tier']

    def get_model_perms(self, request):
        return {}

admin.site.register(models.OrganizationAgreement, OrganizationAgreementAdmin)


class OrganizationContactInline(admin.TabularInline):
    model = models.OrganizationContact
    extra = 0
    ordering = ('user',)
    fields = ('user', 'phone_number',)


class OrganizationAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['code', 'name', 'description', 'type', 'phone_number']}),
        ('Address',             {'fields': ['street', 'city', 'state', 'zip_code', 'country']}),
        ('Agreement',           {'fields': ['agreement']})
    ]

    list_display = ('code', 'name', 'description', 'type', 'phone_number')
    search_fields = ['code', 'name', 'description', 'type', 'phone_number']
    list_filter = ['type']

    inlines = [OrganizationContactInline]

admin.site.register(models.Organization, OrganizationAdmin)


class ReferralCodeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['code', 'description', 'creation_date']}),
        ('Active',              {'fields': ['expiration_date', 'is_active']}),
        ('Organization',        {'fields': ['organization']})
    ]

    list_display = ('code', 'description', 'organization')
    search_fields = ['code', 'description', 'organization']
    list_filter = ['organization']

admin.site.register(models.ReferralCode, ReferralCodeAdmin)


class CommentAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['commenter', 'commentee']}),
        ('Comment',             {'fields': ['comment', 'created']}),
    ]

    list_display = ('commenter', 'commentee')
    search_fields = ['commenter', 'commentee']
    list_filter = ['commenter', 'commentee']

    readonly_fields = models.Comment._meta.get_all_field_names()

admin.site.register(models.Comment, CommentAdmin)


class StudentContributionInline(admin.StackedInline):
    model = models.Contribution
    extra = 0
    ordering = ('creation_datetime',)
    fields = ('backer', 'amount', 'contribution_type', 'is_completed', 'stripe_customer_payment', 'is_recurring', 'comment')
    readonly_fields = models.Contribution._meta.get_all_field_names()
    def has_delete_permission(self, request, obj=None):
        return False


class BackerContributionInline(admin.StackedInline):
    model = models.Contribution
    extra = 0
    ordering = ('creation_datetime',)
    fields = ('student', 'amount', 'contribution_type', 'is_completed', 'stripe_customer_payment', 'is_recurring', 'comment')

    readonly_fields = models.Contribution._meta.get_all_field_names()

    def has_delete_permission(self, request, obj=None):
        return False


class ProfileSocialNetworkInline(admin.TabularInline):
    model = models.ProfileSocialNetwork
    extra = 0
    ordering = ('social_network_type',)
    fields = ('social_network_type', 'is_connected', 'link',)


class CommenteeCommentInline(admin.TabularInline):
    model = models.Comment
    extra = 0
    ordering = ('-created',)
    fields = ('commenter', 'comment')
    fk_name = 'commentee'
    readonly_fields = ['commenter', 'comment']
    verbose_name = "Comment from others to the user"
    verbose_name_plural = "Comments from others to the user"

    readonly_fields = models.Comment._meta.get_all_field_names()


class CommenterCommentInline(admin.TabularInline):
    model = models.Comment
    extra = 0
    ordering = ('-created',)
    fields = ('commentee', 'comment')
    fk_name = 'commenter'
    verbose_name = "Comment"
    verbose_name_plural = "Comments"

    readonly_fields = models.Comment._meta.get_all_field_names()


class UserProfileAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['user']}),
        ('Address',             {'fields': ['street', 'city', 'state', 'zip_code', 'country']}),
        ('Images',              {'fields': ['cover_image', 'profile_picture']}),
        ('Others',              {'fields': ['link', 'user_type', 'referral_code', 'has_agreed_to_terms', 'birthday']})
    ]

    list_display = ('user',)
    search_fields = ['user']

    inlines = [ProfileSocialNetworkInline, CommenteeCommentInline, CommenterCommentInline]

    readonly_fields = models.UserProfile._meta.get_all_field_names()

admin.site.register(models.UserProfile, UserProfileAdmin)


class StudentLoanDocumentInline(admin.TabularInline):
    model = models.StudentLoanDocument
    extra = 0
    ordering = ('uploaded_datetime',)
    fields = ('document', 'is_verified')


class StudentLoanAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['student']}),
        ('Loan Information',    {'fields': ['amount_owed', 'is_loan_statement_uploaded', 'loan_provider']}),
    ]

    list_display = ('student', 'amount_owed', 'is_loan_statement_uploaded',)

    inlines = [StudentLoanDocumentInline]

    actions = ['download_loan_documents']

    def download_loan_documents(self, request, queryset):

        files = {}
        for student_loan in queryset:
            directory = student_loan.student.user_profile.user.email + " - (" + student_loan.student.user_profile.link + ")"
            files[directory] = []
            for document in student_loan.documents.all():
                if os.path.isfile(document.document.path):
                    files[directory].append(document.document.path)

        return Utility.zip_and_download_files(
            file_paths=files,
            zip_file_name="student-loans(%s)" % datetime.datetime.now().strftime("%m-%d-%Y %H-%M"))

    download_loan_documents.short_description = "Download loan documents for selected student loans."

    readonly_fields = models.StudentLoan._meta.get_all_field_names()

admin.site.register(models.StudentLoan, StudentLoanAdmin)


class StudentLoanInline(admin.TabularInline):
    model = models.StudentLoan
    extra = 0
    ordering = ('amount_owed',)
    fields = ('amount_owed', 'is_loan_statement_uploaded', 'loan_provider',)


class StudentAccount529Inline(admin.StackedInline):
    model = models.StudentAccount529
    fields = ('parent_full_name',
              'parent_phone_number', 'parent_email', 'bank_name',
              'full_name_on_account', 'account_number')

    readonly_fields = models.StudentAccount529._meta.get_all_field_names()


class StudentTuitionInline(admin.StackedInline):
    model = models.StudentTuition
    extra = 0
    ordering = ('amount',)
    fields = ('amount', 'description',
              'is_paid', 'paid_date', 'creation_date',
              'end_date')

    readonly_fields = models.StudentTuition._meta.get_all_field_names()


class StudentCollegeInline(admin.StackedInline):
    model = models.StudentCollege
    extra = 0
    ordering = ('start_date',)
    fields = ('name', 'institution', 'order', 'start_date',
              'end_date', 'season', 'degree',
              'major', 'major_name', 'subjects', 'projects', 'story', 'is_in_progress')

    readonly_fields = models.StudentCollege._meta.get_all_field_names()


class StudentHighSchoolInline(admin.StackedInline):
    model = models.StudentHighSchool
    extra = 0
    ordering = ('start_date',)
    fields = ('name', 'institution', 'start_date',
              'end_date', 'subjects', 'projects', 'story', 'is_in_progress')

    readonly_fields = models.StudentHighSchool._meta.get_all_field_names()


class StudentAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['user_profile']}),
        ('Education Status',    {'fields': ['overall_gpa', 'is_high_school_student', 'is_college_student']}),
        ('Others',              {'fields': ['is_featured', 'display_order', 'goal_amount', 'grad_story', 'hobbies',
                                            'volunteer_projects_internships', 'more_about_me']}),
    ]

    list_display = ('user_profile',)
    search_fields = ['user_profile__user__first_name']

    inlines = [StudentAccount529Inline,
               StudentLoanInline,
               StudentTuitionInline,
               StudentCollegeInline,
               StudentHighSchoolInline,
               StudentContributionInline]

    readonly_fields = ['user_profile', 'overall_gpa', 'is_high_school_student', 'is_college_student', 'grad_story',
                       'hobbies', 'volunteer_projects_internships', 'more_about_me']

admin.site.register(models.Student, StudentAdmin)


class BackerAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['user_profile']}),
    ]

    list_display = ('user_profile',)
    search_fields = ['user_profile__user__first_name']

    inlines = [BackerContributionInline]

    readonly_fields = models.Backer._meta.get_all_field_names()

admin.site.register(models.Backer, BackerAdmin)


class NotificationAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['type', 'student', 'is_read']}),
        ('Details',             {'fields': ['description']})
    ]

    list_display = ('type', 'student', 'is_read')
    search_fields = ['student__user_profile__user__first_name',
                     'student__user_profile__user__last_name',
                     'student__user_profile__user__email']
    list_filter = ['is_read', 'type']

    readonly_fields = ['type', 'student', 'description']

admin.site.register(models.Notification, NotificationAdmin)


class StripeCustomerPaymentAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['user', 'stripe_customer_id', 'is_most_recent', 'email', 'client_ip',
                                            'created', 'stripe_token', 'country', 'name', 'fingerprint']}),
        ('Address',             {'fields': ['address_street', 'address_city', 'address_state',
                                            'address_zip_code', 'address_country']}),
        ('Card',                {'fields': ['brand', 'cvc_check', 'expiration_month', 'expiration_year',
                                            'funding', 'card_id', 'last_4_digits']})
    ]

    list_display = ('name', 'brand', 'card_id', 'user')

    list_filter = ['brand']

    readonly_fields = models.StripeCustomerPayment._meta.get_all_field_names()

admin.site.register(models.StripeCustomerPayment, StripeCustomerPaymentAdmin)


class ContactAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                  {'fields': ['user_profile', 'name', 'email', 'created', 'is_read']}),
        ('Message',             {'fields': ['subject', 'message']}),
    ]

    list_display = ('user_profile', 'email', 'created', 'subject', 'is_read')

    readonly_fields = ['user_profile', 'name', 'email', 'created', 'subject', 'message']

admin.site.register(models.Contact, ContactAdmin)


class BlogAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())
    summary = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = models.Blog
        fields = ['title', 'link', 'writer_name', 'lunch_datetime', 'image', 'summary', 'content']


class BlogAdmin(admin.ModelAdmin):
    form = BlogAdminForm

    list_display = ['title', 'writer_name', 'link', 'lunch_datetime']

admin.site.register(models.Blog, BlogAdmin)
