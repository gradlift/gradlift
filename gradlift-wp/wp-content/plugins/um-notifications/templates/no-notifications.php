<div class="um-notification-header">
	<div class="um-notification-left"><?php _e('Notifications','um-notifications'); ?></div>
	<div class="um-notification-right"><a href="<?php echo $ultimatemember->account->tab_link( 'webnotifications' ); ?>" class="um-notification-i-settings"><i class="um-faicon-cog"></i></a></div>
	<div class="um-clear"></div>
</div>

<div class="um-notification-ajax">

	<p class="um-notification none"><?php _e('You do not have any notifications yet.','um-notifications'); ?></p>
	
</div>