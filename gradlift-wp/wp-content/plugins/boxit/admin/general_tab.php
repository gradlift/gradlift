<?php




function general_tab_fields() {
    $new = '<span style="color: #fff; background: #32C425; display: inline-block; padding: 3px 5px; margin-left: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; font-size: 12px;">New</span>';
    $fields = array(
        array(
            'id' => 'dropbox_authentication',
            'title' => '',
            'fields' => array()
        ),
        array(
            'id' => 'files_destination',
            'title' => 'Destination for uploaded files',
            'fields' => array(
                array(
                    'id' => 'user_folder',
                    'title' => 'User folder:<br>If user is logged in store uploaded files in "his" folder?'
                ),
                array(
                    'id' => 'name_folder',
                    'title' => 'Name folder:<br>If "name/email" fields are enabled store files in folder from "name" input?'
                ),
                array(
                    'id' => 'organize',
                    'title' => 'Organize uploads into day, month, year based folders?'
                )
            )
        ),
        array(
            'id' => 'restrictions',
            'title' => 'Access & upload restrictions',
            'fields' => array(
                array(
                    'id' => 'only_for_logged_in',
                    'title' => 'Who can use BOXIT?'
                ),
                array(
                    'id' => 'name_email',
                    'title' => 'Enable the "name & email" fields?'
                ),
                array(
                    'id' => 'upload_message',
                    'title' => 'Enable the "upload comment" field?'
                ),
                array(
                    'id' => 'captcha',
                    'title' => 'Enable CAPTCHA?'
                ),
                array(
                    'id' => 'size_limit',
                    'title' => 'File size limit:'
                ),
                array(
                    'id' => 'chunk',
                    'title' => 'Split uploaded files to chunks:'
                ),
                array(
                    'id' => 'filetypes',
                    'title' => 'Allowed file extensions:'
                ),
                array(
                    'id' => 'hide_on_start',
                    'title' => 'Hide "start upload" button & "drop zone" when upload starts'.$new,
                )
            )
        ),
        array(
            'id' => 'notifications',
            'title' => 'Email notifications',
            'fields' => array(
                array(
                    'id' => 'email_notification',
                    'title' => 'Email notification on upload?'
                ),
                array(
                    'id' => 'notifications_address',
                    'title' => 'Email address for upload notification:'
                ),
                array(
                    'id' => 'extra_notification',
                    'title' => 'Additional email notification for person who uploaded files?'
                )
            )
        ),
        array(
            'id' => 'thankyou',
            'title' => 'Thank you message / page',
            'fields' => array(
                array(
                    'id' => 'thank_you',
                    'title' => 'Enable thank you message'.$new
                ),array(
                    'id' => 'thankyou_page',
                    'title' => 'Custom "thank you" page URL'.$new
                )
            )
        ),
        array(
            'id' => 'debug',
            'title' => 'Debug info',
            'fields' => array(
                array(
                    'id' => 'debug_console',
                    'title' => 'Enable debug log?'
                )
            )
        )
    );
    return $fields;
}






function boxit_dropbox_authentication_section() {
    echo '<input type="hidden" name="boxit_tab" value="general"/>';
    $status = boxit_status();
    if($status)  {
        //echo "<pre>"; print_r($status); echo "</pre>";
        echo '<div style="margin-left:0px;" class="updated"><p>';
        echo 'BOXIT is connected to the following Dropbox account:<br>';
        echo 'Account email: '.$status['email'].'<br>';
        echo 'User display name: '.$status['display_name'].'<br>';
        echo '</p></div>';
        //reinstall
        echo '<br><a href="'.admin_url('options-general.php?page=boxit_settings&reinstall_boxit').'" class="button">Connect BOXIT to different Dropbox account (reinstall)</a><br><br>';
    } else {
        echo '<div style="margin-left:0px;" class="error"><p>Could not establish connection with Dropbox API</p></div>';
        echo '<br><a href="'.admin_url('options-general.php?page=boxit_settings&reinstall_boxit').'" class="button">Run BOXIT instalation.</a><br><br>';
    }

    //var_dump('max_execution_time '.ini_get('max_execution_time'));
    //var_dump('max_input_time '.ini_get('max_input_time'));
    //var_dump('memory_limit: '.ini_get('memory_limit'));
}

function boxit_dropbox_authentication_dropbox_login() {

}

function boxit_dropbox_authentication_dropbox_pass() {

}










function boxit_files_destination_section() {
    ?>
    <p>Files uploaded to Dropbox will be stored in <strong>Applications/your_app_name/</strong></p>
    <?php
}



function boxit_files_destination_user_folder() {
    $options = get_option('boxit_options');
    ?>
    <select name="boxit_options[user_folder]" id="user_folder">
        <option <?php if($options['user_folder'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['user_folder'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>Ex. Files from user "john_doe" will be stored in  <strong>Dropbox/Applications/your_app_name/john_doe</strong></i>
    </p>
    <?php
}



function boxit_files_destination_name_folder() {
    $options = get_option('boxit_options');
    if(!isset($options['name_folder'])) $options['name_folder'] = 0;
    ?>
    <select name="boxit_options[name_folder]" id="name_folder">
        <option <?php if($options['name_folder'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['name_folder'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>Ex. If someone inputs "john" to "name field" his files will be stored in  <strong>Dropbox/Applications/your_app_name/john</strong></i>
    </p>
    <?php
}



function boxit_files_destination_organize() {
    $options = get_option('boxit_options');
    if(!isset($options['organize'])) $options['organize'] = 0;
    ?>
    <select name="boxit_options[organize]" id="organize">
        <option <?php if($options['organize'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['organize'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>If set to Yes it will store files in <strong>Dropbox/Applications/your_app_name/<?php echo date('Y') ?>/<?php echo date('m') ?>/<?php echo date('d') ?></strong>. If you have the "name folder" or "user folder" enabled files will be stored in <strong>Dropbox/Applications/your_app_name/<?php echo date('Y') ?>/<?php echo date('m') ?>/<?php echo date('d') ?>/name_path</strong></i>
    </p>
    <?php
}









function boxit_restrictions_section() {
}

function boxit_restrictions_only_for_logged_in() {
    $options = get_option('boxit_options');
    ?>
    <select name="boxit_options[only_for_logged_in]" id="only_for_logged_in">
        <option <?php if($options['only_for_logged_in'] == '1') echo 'selected="selected"'; ?> value="1">Only logged in users</option>
        <option <?php if($options['only_for_logged_in'] == '0') echo 'selected="selected"'; ?>value="0">Everyone</option>
    </select>
    <?php
}

function boxit_restrictions_name_email() {
    $options = get_option('boxit_options');
    ?>
    <select name="boxit_options[name_email]" id="name_email">
        <option <?php if($options['name_email'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['name_email'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>Users will have to input their name & email before they can use BOXIT <br>IF user is logged in name/email fields will be pre filled with user login & email </i>
    </p>
    <?php
}

function boxit_restrictions_upload_message() {
    $options = get_option('boxit_options');
    ?>
    <select name="boxit_options[upload_message]" id="upload_message">
        <option <?php if($options['upload_message'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['upload_message'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>Users will have the opportunity to input optional "message/comment" before uploading files.<br>The "message / comment" can be used in notification email.</i>
    </p>
    <?php
}


function boxit_restrictions_captcha() {
    $options = get_option('boxit_options');
    ?>
    <select name="boxit_options[captcha]" id="captcha">
        <option <?php if($options['captcha'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['captcha'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>Add CAPTCHA field to BOXIT</i>
    </p>
    <?php
}


function boxit_restrictions_size_limit() {
    $options = get_option('boxit_options');
    $file_size = array(
              "1000000" => '1MB per file',
              "5000000" => '5MB per file',
              "8000000" => '8MB per file',
              "16000000" => '16MB per file',
              "32000000" => '32MB per file',
              "64000000" => '64MB per file',
              "100000000" => '100MB per file',
              "128000000" => '128MB per file',
              "256000000" => '256MB per file',
              "500000000" => '500MB per file',
              "1000000000"  => '1GB per file',
              "2000000000"  => '2GB per file'
        );
    ?>
    <select name="boxit_options[size_limit]" id="boxit_size_limit">
        <?php
        foreach( $file_size as $key => $val) {
            echo '<option value="'.$key.'" '.selected($key,$options['size_limit']).'>'.$val.'</option>';
        }
        ?>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>NOTE: Setting this for example to 128MB will only mean that files larger than 128MB will be rejected by upload script - it WILL NOT ENABLE uploading a 128MB files if the server upload limits are set lower. <br> If You need to transfer larger files than Your server settings currently allow - contact Your hosting provider and ask for ability to send larger files.</i>
        <br>
        <i style="color:#307DA1;">Your current server settings: <strong>post_max_size: </strong><?php echo pretty_size(toBytes(ini_get('post_max_size'))) ?>, <strong>upload_max_filesize: </strong><?php echo pretty_size(toBytes(ini_get('upload_max_filesize'))) ?></i>
    </p>
    <?php
}



function boxit_restrictions_chunk() {
    if(!isset($options['chunk'])) $options['chunk'] = '1000000';
    $options = get_option('boxit_options');
    $chunks = array(
              "1000000" => '1MB',
              "5000000" => '4MB',
              "8000000" => '8MB',
              "16000000" => '16MB',
              "32000000" => '32MB'
        );
    ?>
    <select name="boxit_options[chunk]" id="boxit_chunk">
        <?php
        foreach( $chunks as $key => $val) {
            echo '<option value="'.$key.'" '.selected($key,$options['chunk']).'>'.$val.'</option>';
        }
        ?>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>Split uploaded file into chunks.</i>
    </p>
    <?php
}

function boxit_restrictions_filetypes() {
    $options = get_option('boxit_options');
    ?>
    <input name="boxit_options[filetypes]" autocomplete="off" type="text" id="boxit_filetypes" size="50" value="<?php echo $options['filetypes'] ?>" />
    <div class="boxit_explain"></div>
    <p>
        <i>File extensions coma separated (jpg, gif, png, zip) <br> Leave blank to allow all file types.<br> <span style="color:red">NOTE:</span> only filetypes allowed by WordPress will be accepted (<a href="https://codex.wordpress.org/Function_Reference/get_allowed_mime_types" target="_blank">list of accepted filetypes</a>)</i>
    </p>
    <?php
}


function boxit_restrictions_hide_on_start() {
    $options = get_option('boxit_options');
    if(!isset($options['hide_on_start'])) $options['hide_on_start'] = 0;
    ?>
    <select name="boxit_options[hide_on_start]" id="hide_on_start">
        <option <?php if($options['hide_on_start'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['hide_on_start'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>If enabled "start upload" button & "drop zone" will be hidden after upload starts</i>
    </p>
    <?php
}









function boxit_notifications_section() {
}

function boxit_notifications_email_notification() {
    $options = get_option('boxit_options');
    ?>
    <select name="boxit_options[email_notification]" id="email_notification">
        <option <?php if($options['email_notification'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['email_notification'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>The notification email (see Emails tab) will be sent after all files are uploaded to Dropbox</i>
    </p>
    <?php
}

function boxit_notifications_notifications_address() {
    $options = get_option('boxit_options');
    ?>
    <input name="boxit_options[notifications_address]" autocomplete="off" type="text" id="notifications_address"  value="<?php echo $options['notifications_address'] ?>" />
    <div class="boxit_explain"></div>
    <p>
        <i>Notification email will be sent to this address.</i>
    </p>
    <?php
}

function boxit_notifications_extra_notification() {
    $options = get_option('boxit_options');
    ?>
    <select name="boxit_options[extra_notification]" id="extra_notification">
        <option <?php if($options['extra_notification'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['extra_notification'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>NOTE: for the additional notification to work You should have <strong>ONE</strong> of the following settings:<br>
        - The <strong>"Who can use BOXIT?"</strong> option set to <strong>"Only logged in users"</strong> (logged in user email will be used)<br>
        - The <strong>"Enable the 'name & email' fields?"</strong> option set to <strong>"Yes"</strong> (address from "email" field will be used)</i>
    </p>
    <?php
}




function boxit_debug_section() {
}

function boxit_debug_debug_console() {
    $options = get_option('boxit_options');
    if(!isset($options['debug_console'])) $options['debug_console'] = 0;
    ?>
    <select name="boxit_options[debug_console]" id="debug_console">
        <option <?php if($options['debug_console'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['debug_console'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>Debug log will be available in the console (web developers toolbar).</i>
    </p>
    <?php
}





function boxit_thankyou_section() {
}

function boxit_thankyou_thank_you() {
    $options = get_option('boxit_options');
    if(!isset($options['thank_you'])) $options['thank_you'] = 0;
    ?>
    <select name="boxit_options[thank_you]" id="thank_you">
        <option <?php if($options['thank_you'] == '1') echo 'selected="selected"'; ?> value="1">Yes</option>
        <option <?php if($options['thank_you'] == '0') echo 'selected="selected"'; ?>value="0">No</option>
    </select>
    <div class="boxit_explain"></div>
    <p>
        <i>"Thank you" message will be displayed after all files from the queue are successfully uploaded. After "thank you" message is shown selecting and uploading files will no longer be possible. <br>"Thank you" message text can be edited in "Translation" tab (BOXIT settings).</i>
    </p>
    <?php
}

function boxit_thankyou_thankyou_page() {
    $options = get_option('boxit_options');
    if(!isset($options['thankyou_page'])) $options['thankyou_page'] = '';
    ?>
    <input name="boxit_options[thankyou_page]" autocomplete="off" type="text" id="thankyou_page" size="50" value="<?php echo $options['thankyou_page'] ?>" />
    <div class="boxit_explain"></div>
    <p>
        <i>User will be redirected to given URL after all files are successfully uploaded.</i>
    </p>
    <?php
}

