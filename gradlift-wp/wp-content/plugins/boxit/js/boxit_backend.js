jQuery(document).ready(function($) {


	$('#boxit_primary_color_color_picker').wpColorPicker({defaultColor:'#03C4A6'});

	$('#boxit_secondary_color_color_picker').wpColorPicker({defaultColor:'#C7C7C7'});

	$('#boxit_text_on_primary_color_picker').wpColorPicker({defaultColor:'#ffffff'});


	$('.boxit_explain').each(function() {
		$(this).next('p').hide();
	})
	$('.boxit_explain').on('click', function() {
		var t = $(this),
			span = t.next('p');
		span.slideToggle(200);
	});

	//adjust the translation tab
	if($('body.settings_page_boxit_settings .nav-tab[href="?page=boxit_settings&tab=translation"]').hasClass('nav-tab-active')) {
		$('.form-table th').remove();
	}


});