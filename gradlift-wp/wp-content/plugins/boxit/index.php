<?php
/*
Plugin Name: BOXIT 2
Plugin URI: http://codecanyon.net/user/sommersethdesign
Description: Dropbox file uploader for Wordpress
Version: 2.7
Author: Konrad Węgrzyniak
Author URI: http://codecanyon.net/user/sommersethdesign
Copyright: Sommerseth Design
*/




//Plugin auto update
//http://w-shadow.com/blog/2010/09/02/automatic-updates-for-any-plugin/
if(!class_exists( "PluginUpdateChecker_1_6" )) {
    require('plugin-updates/plugin-update-checker.php');
}
$boxit_update = new PluginUpdateChecker_1_6(
    'http://updates.sommersethdesign.no/plugin_upgrade/boxit/info.json',
    __FILE__,
    'boxit' //plugin slug
);



//create config file if it does not exits
function config_file() {
    if(!file_exists(dirname(__FILE__)."/app-info.json")) {
        $content = get_option('dropbox_key_secret');
        if(!$content) {
            return false;
        } else {
            $fp = fopen(dirname(__FILE__)."/app-info.json","wb");
            fwrite($fp,$content);
            fclose($fp);
            return true;
        }
    } else {
        return true;
    }
}
config_file();



//used in boxit/settings.php douring boxit reinstalation
function delete_config_file() {
    if(file_exists(dirname(__FILE__)."/app-info.json")) {
        @unlink(dirname(__FILE__)."/app-info.json");
    }
}





require_once "dropbox-sdk/Dropbox/autoload.php";
require_once "app-connect.php";
require('strings.php');
require('admin/settings.php');

use \Dropbox as dbx;






/**
 * Add scripts and styles for the frontend
 */
function boxit_frontend_scripts_and_styles() {
    global $__trans;
    wp_enqueue_script('jquery');
    /*
    wp_deregister_script('jquery');
   wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://code.jquery.com/jquery-1.11.0.min.js", false, null);
   wp_enqueue_script('jquery');
    wp_enqueue_script( 'boxit_plupload', plugins_url('js/plupload/plupload.full.min.js', __FILE__ ),array( 'jquery' ));
     */
    wp_enqueue_script( 'rp_plugin', plugins_url('js/realperson/jquery.plugin.js', __FILE__ ),array( 'jquery' ));
    wp_enqueue_script( 'rp', plugins_url('js/realperson/jquery.realperson.js', __FILE__ ),array( 'jquery' ));

    wp_enqueue_script( 'boxit_plupload', plugins_url('js/plupload/plupload.full.min.js', __FILE__ ),array( 'jquery' ));
    wp_enqueue_script( 'boxit_moxie', plugins_url('js/plupload/moxie.js', __FILE__ ),array( 'jquery' ));
    wp_enqueue_script( 'boxit_move', plugins_url('js/move_to_dropbox.js', __FILE__ ),array( 'jquery' ));
    wp_localize_script( 'boxit_move', 'boxit_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    wp_localize_script( 'boxit_move', '__trans', $__trans );
    wp_enqueue_script( 'boxit', plugins_url('js/boxit.js', __FILE__ ),array( 'jquery' ));
    wp_localize_script( 'boxit', '__trans', $__trans );
    wp_enqueue_style('boxit_css',plugins_url('boxit.css', __FILE__));
}
add_action( 'wp_enqueue_scripts', 'boxit_frontend_scripts_and_styles' );





/**
 * Add custom CSS for the frontend
 */
add_action( 'wp_head', 'boxit_custom_css' );
function boxit_custom_css() {
    $options = get_option('boxit_options');
    ?>
    <style type="text/css" media="screen">
        .boxit_col_1 { color:<?php echo $options['primary_color'] ?>!important; }
        .boxit_bgd_1 { background:<?php echo $options['primary_color'] ?>!important; }
        .boxit_border_1 { border: 1px solid <?php echo $options['primary_color'] ?>!important;}
        .boxit_text_1 { color:<?php echo $options['text_on_primary'] ?>!important;}
        .boxit_col_2 { color:<?php echo $options['secondary_color'] ?>!important; }
        .boxit_bgd_2 { background:<?php echo $options['secondary_color'] ?>!important; }
        .boxit_border_2 { border: 1px solid <?php echo $options['secondary_color'] ?>!important;}

        #boxit_dropzone {  border: 5px dashed <?php echo $options['secondary_color'] ?>!important;}
        #boxit_dropzone.drop_area_active { border:5px dashed <?php echo $options['primary_color'] ?>!important; }
        #boxit_dropzone.drop_area_active .boxit_icon { color:<?php echo $options['primary_color'] ?>!important; }
    }
    </style>
    <?php
}



/**
 * Add scripts and styles for the backend
 */
function boxit_backend_scripts_and_styles() {
    wp_enqueue_script('jquery');
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'boxit-backend-js', plugins_url('js/boxit_backend.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
}
add_action( 'admin_enqueue_scripts', 'boxit_backend_scripts_and_styles' );




/**
 * Add custom CSS for the backend
 */
add_action( 'admin_head', 'boxit_custom_backend_css' );
function boxit_custom_backend_css() {
    ?>
    <style type="text/css">
        .boxit_explain {
            display: inline-block;
            width:20px;
            height:20px;
            margin-left: 5px;
            margin-bottom: -6px;
            background: url(<?php echo plugins_url('img/helper.png', __FILE__ ) ?>) bottom center no-repeat;
            cursor: pointer;
        }
    </style>
    <?php
}





/**
 * Add the shortcode
 */
function shortcode_action($atts) {
    $atts = shortcode_atts(
        array ('custom_folder' => '-'),
        $atts
    );
    return boxit($atts);
}
add_shortcode('boxit', 'shortcode_action');




/**
 * function triggered by the shortcode
 * @param  boolean $atts [description]
 * @param  boolean $echo [description]
 * @return [type]        [description]
 */
function boxit($atts = false) {
    $options = get_option('boxit_options');

    global $__trans;
    //if dropbox connection is not set up
    if(!get_option('dropbox_auth_code') || !get_option('dropbox_access_token') || !config_file()) return '<p>'.$__trans['setup_api_con'].'</p>';
    //if only for logged in and user not logged in(or visitor)
    if($options['only_for_logged_in'] && !is_user_logged_in()) return '<p>'.$__trans['only_loggedin'].'</p>';

    return boxit_uploader($atts);
}




/**
 * Generates the BOXIT HTML wrapper and its content (for main boxit() function)
 * @return HTML The BOXIT
 */
function boxit_uploader($atts = false) {
    global $__trans;
    $shortcode_folder = rtrim(ltrim(trim($atts['custom_folder']),"/"),"/");
    $boxit_options = get_option('boxit_options');
    $filetypes = ($boxit_options['filetypes'] != '')? '.'.str_replace(',', ', .', $boxit_options['filetypes']) : $__trans['all_files'];
    $pre_upload = ($boxit_options['name_email'] || $boxit_options['upload_message'])? 'pre_upload' : '';
    $debug = (isset($boxit_options['debug_console']))? $boxit_options['debug_console'] : 0;
    $thank_you = (isset($boxit_options['thank_you']))? $boxit_options['thank_you'] : 0;
    $thankyou_page = (isset($boxit_options['thankyou_page']))? $boxit_options['thankyou_page'] : '';
    $hide_on_start = (isset($boxit_options['hide_on_start']))? $boxit_options['hide_on_start'] : 0;
    $send_email = ($boxit_options['extra_notification'] || $boxit_options['email_notification'])? 1 : 0;
    $organize = (isset($boxit_options['organize']))? $boxit_options['organize'] : 0;
    $chunk = (isset($boxit_options['chunk']))? $boxit_options['chunk'] : '1000000';
    $b = '';
    $hide_space = (isset($boxit_options['hide_space']))? $boxit_options['hide_space'] : '0';

    if($pre_upload != '') $b .= boxit_pre_upload();
    $b .= '<div id="boxit" class="'.$pre_upload.'" data-hide_on_start="'.$hide_on_start.'" data-thank_you="'.$thank_you.'" data-thankyou_page="'.$thankyou_page.'" data-debug="'.$debug.'" data-upload_script="'.plugins_url('upload.php', __FILE__ ).'" data-size_limit="'.boxit_filesize_format($boxit_options['size_limit']).'" data-file_types="'.$boxit_options['filetypes'].'" data-shortcode_folder="'.$shortcode_folder.'" data-organize="'.$organize.'" data-email_notification="'.$send_email.'" data-chunk="'.$chunk.'" data-hide_space="'.$hide_space.'">';
        $b .= '<div id="boxit_dropzone">';
            if(!$boxit_options['hide_logo']) : $b .= '<span class="boxit_icon boxit_col_2">b</span>'; endif;
            $b .= '<span class="drop_info boxit_col_2">'.$__trans['drop_here'].'</span>';
            $b .= '<span class="release_info boxit_col_1">'.$__trans['release'].'</span>';
            $b .= '<div id="boxit_browse" class="boxit_text_1 boxit_bgd_1" href="javascript:;">'.$__trans['select'].'</div>';
            $b .= '<span class="filetypes boxit_col_1">'.$__trans['allowed'].'</span>';
            $b .= '<span class="filetypes boxit_col_1">'.$filetypes.'<br>('.$__trans['max'].' '.boxit_filesize_format($boxit_options['size_limit']).')</span>';
            $b .= '<div class="space_status boxit_col_1"></div>';
        $b .= '</div>';
        $b .= '<p id="queue_header" class="boxit_col_2">'.$__trans['queue'].'</p>';
        $b .= '<ul id="filelist"></ul>';
        $b .= '<div class="boxit_start_upload_container">';
            $b .= '<a id="boxit_start_upload" class="boxit_text_1 boxit_bgd_1" href="javascript:;">'.$__trans['start'].'</a>';
        $b .= '</div>';
    $b .= '</div>';
    return $b;
}





/**
 * If "name / email" or "upload message" is enabled ...
 * Created the "name / email" OR/AND "upload message" input fields taht need to be filed to access BOXIT
 * @return HTML the "name / email" OR/AND "upload message" fields
 */
function boxit_pre_upload() {
    global $__trans;
    $boxit_options = get_option('boxit_options');
    $name = (is_user_logged_in())? wp_get_current_user()->user_login : '';
    $email = (is_user_logged_in())? wp_get_current_user()->user_email : '';

    $b = '<div id="boxit_pre_upload">';
        if(!$boxit_options['hide_logo']) : $b .= '<span class="boxit_icon boxit_col_2">b</span>'; endif;

        if($boxit_options['name_email']) {
            $b .= '<div id="name_email">'.
                '<label for="boxit_name_field">'.
                    '<span class="boxit_col_2">'.$__trans['name'].'</span>'.
                    '<input type="text" name="boxit_name" value="'.$name.'" id="boxit_name_field" class="boxit_name boxit_col_2 boxit_border_2">'.
                '</label>'.
                '<label for="boxit_email_field">'.
                    '<span class="boxit_col_2">'.$__trans['email'].'</span>'.
                    '<input type="text" name="boxit_email" value="'.$email.'" id="boxit_email_field" class="boxit_email boxit_col_2 boxit_border_2">'.
                '</label>'.
            '</div>';
        }

        if($boxit_options['upload_message']) {
            $b .= '<div id="upload_message">'.
                '<label for="upload_message_field">'.
                    '<span class="boxit_col_2">'.$__trans['message'].'</span>'.
                    '<textarea name="upload_message" class="boxit_upload_message boxit_col_2 boxit_border_2" id="upload_message_field"></textarea>'.
                '</label>'.
            '</div>';
        }

        if(isset($boxit_options['captcha'])) {
            if($boxit_options['captcha']) {
                $b .= '<form id="boxit_captcha" method="POST">'
                        .'<label for="captcha_text">'
                            .'<span class="boxit_col_2">'.$__trans['captcha'].'</span>'
                            .'<input type="text" name="captcha" class="boxit_border_2" id="boxit_captcha_text" autocomplete="off" />'
                        .'</label>'
                        .'<input type="submit" id="bc_sub">'
                    .'</form>';
            }
        }


        $b .= '<div id="show_boxit" class="boxit_text_1 boxit_bgd_1" href="javascript:;">'.$__trans['proceed'].'</div>';
    $b .= '</div>';
    return $b;
}



/**
 * Displays available space on Dropbox account
 * Called by AJAX
 */
function boxit_space_status() {
    global $__trans;
    if(get_option('dropbox_access_token')) {
        $dbxClient = new dbx\Client(get_option('dropbox_access_token'), "PHP-Example/1.0");
        try {
            $accountInfo = $dbxClient->getAccountInfo();
        } catch (Exception $e) {
            echo '<p style="color:red">'.$e->getMessage().'</p>';
        }
        $s = '';
        //echo "<pre>"; print_r($accountInfo); echo "</pre>";
        if($accountInfo !== NULL) {
            $total = floor($accountInfo['quota_info']['quota']/1048576);
            $used = floor(($accountInfo['quota_info']['normal'] + $accountInfo['quota_info']['shared'])/1048576);
            $available = floor(($accountInfo['quota_info']['quota'] - $accountInfo['quota_info']['normal'] - $accountInfo['quota_info']['shared'])/1048576);
            //echo $total.'<br>';
            //echo $used.'<br>';
            //echo $available.'<br>';
            if($available >= 1024) {
                $available = $available/1024;
                $a = number_format($available, 3, ',', ' ').' GB';
            } else {
                $a = $available.' MB';
            }
            $s .= '<div class="boxit_bgd_2"><span class="boxit_bgd_1" style="width:'.(($used/$total)*100).'%"></span></div>';
            $s .= '<span class="boxit_col_1 status_label">'.$a.' '.$__trans['available_on_d'].'</span>';
            echo $s;
        }
    } else {
        echo '<p style="color:red">dropbox_access_token missing</p>';
    }
    die();
}
add_action( 'wp_ajax_nopriv_boxit_space_status', 'boxit_space_status' );
add_action( 'wp_ajax_boxit_space_status', 'boxit_space_status' );




/**
 * Validating BOXIT captcha
 * Validate function called by AJAX
 * Takes the hashed captha value and compares it with user input using "real person" captha jquery plugin tools (functions)
 */
function boxit_check_captcha() {
    $hashed_captcha = $_POST['captchaHash'];
    if(PHP_INT_SIZE == 8) {
        //captcha hash check on 64 bit PHP
        $user_input = rp_check_hash_64($_POST['captcha']);
    } else {
        //captcha hash check on 32 bit PHP
        $user_input = rp_check_hash_32($_POST['captcha']);
    }
    //echo $user_input . ' / '. $hashed_captcha;
    //return test value to AJAX function in js.
    echo ($user_input == $hashed_captcha)? 'ok' : '0';
    die();
}
add_action( 'wp_ajax_nopriv_boxit_check_captcha', 'boxit_check_captcha' );
add_action( 'wp_ajax_boxit_check_captcha', 'boxit_check_captcha' );
/**
 * Hashing user captcha input on 64 bit PHP
 */
function rp_check_hash_64($value) {
    $hash = 5381;
    $value = strtoupper($value);
    for($i = 0; $i < strlen($value); $i++) {
        $hash = (leftShift32($hash, 5) + $hash) + ord(substr($value, $i));
    }
    return $hash;
}
/**
 * Hashing user captcha input on 32 bit PHP
 */
function rp_check_hash_32($value) {
    $hash = 5381;
    $value = strtoupper($value);
    for($i = 0; $i < strlen($value); $i++) {
        $hash = (($hash << 5) + $hash) + ord(substr($value, $i));
    }
    return $hash;
}
/**
 * Helper function for hashing user captcha input on 32 bit PHP
 * Performs a 32bit left shift
 */
function leftShift32($number, $steps) {
    // convert to binary (string)
    $binary = decbin($number);
    // left-pad with 0's if necessary
    $binary = str_pad($binary, 32, "0", STR_PAD_LEFT);
    // left shift manually
    $binary = $binary.str_repeat("0", $steps);
    // get the last 32 bits
    $binary = substr($binary, strlen($binary) - 32);
    // if it's a positive number return it
    // otherwise return the 2's complement
    return ($binary{0} == "0" ? bindec($binary) :
        -(pow(2, 31) - bindec(substr($binary, 1))));
}





/**
 * Trigger by AJAX call - moves uploaded file to dropbox
 */
function move_to_dropbox() {
    $file_id = $_POST['file_id'];
    $file_name = $_POST['file_name'];
    $shortcode_folder = $_POST['shortcode_folder'];
    $organize_files = $_POST['organize'];

    $boxit_options = get_option('boxit_options');

    $dropbox_folder = '';

    if($organize_files) {
        $dropbox_folder = '/'.date('Y').'/'.date('m').'/'.date('d');
    }

    if($shortcode_folder != '-') {
        $shortcode_folder = str_replace('{date}', date('d-m-Y'), $shortcode_folder);
        $dropbox_folder = $dropbox_folder.'/'.$shortcode_folder;
    }

    if($boxit_options['user_folder'] == 1 && is_user_logged_in()) {
        $user = wp_get_current_user();
        $dropbox_folder = $dropbox_folder.'/'.$user->user_login;
    }

    if($boxit_options['name_folder'] == 1) {
        $dropbox_folder = $dropbox_folder.'/'.$_POST['name_input'];
    }


    //optional folder from shortcode attr...

    $appInfo = dbx\AppInfo::loadFromJsonFile(dirname(__FILE__)."/app-info.json");
    $webAuth = new dbx\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");
    $dbxClient = new dbx\Client(get_option('dropbox_access_token'), "PHP-Example/1.0");
    try {
        $f = @fopen(dirname(__FILE__).'/uploads/'.$file_name, "rb");
        $result = $dbxClient->uploadFile($dropbox_folder.'/'.$file_name, dbx\WriteMode::add(), $f);
        fclose($f);
        @unlink(dirname(__FILE__).'/uploads/'.$file_name);
        $res = array(
            'path' => $result['path'],
            'folder' => $dropbox_folder
            );
        echo json_encode($res);
        die();
    } catch (Exception $e) {
        echo $e->getMessage();
        die();
    }
}
add_action( 'wp_ajax_nopriv_move_to_dropbox', 'move_to_dropbox' );
add_action( 'wp_ajax_move_to_dropbox', 'move_to_dropbox' );






/**
 * Trigger by AJAX call - send email notification
 */
function boxit_email_notification() {
    global $__trans;
    $boxit_options = get_option('boxit_options');
    $files = json_decode(stripslashes($_POST['files']));
    $dropbox_statuses = $_POST['dropbox_statuses'];
    $name_field = ($_POST['name_field'] != '0')? $_POST['name_field'] : '';
    $email_field = ($_POST['email_field'] != '0')? $_POST['email_field'] : '';
    $message_field = ($_POST['message_field'] != '0')? $_POST['message_field'] : '';
    add_filter('wp_mail_content_type',create_function('', 'return "text/html";'));
    $l = '<ul style="margin:20px 0; padding:0;">'; //file list HTML
    $folder = ($_POST['folder'] != '0')? $_POST['folder'] : '-';
    $paths = $_POST['paths']; //paths are required for creating filelinks

    $appInfo = dbx\AppInfo::loadFromJsonFile(dirname(__FILE__)."/app-info.json");
    $webAuth = new dbx\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");
    $dbxClient = new dbx\Client(get_option('dropbox_access_token'), "PHP-Example/1.0");

    //var_dump($files);
    //var_dump($dropbox_statuses);
    //var_dump($name_field);
    //var_dump($email_field);
    //var_dump($message_field);

    //file list
    $i = 0; //filelist tracker
    foreach($files as $file) {
        $file_link = ($paths[$i] != '0')? '<a href="'.$dbxClient->createShareableLink($paths[$i]).'">'.$file->name.'</a>' : $file->name;
        $upload_status = ($file->status == 5)? '<span style="color:green">'.$__trans['upl_to_server'].'</span>' : '<span style="color:red">'.$__trans['upl_to_server_err'].'</span>';
        $dropbox_status = ($dropbox_statuses[$i] == 'uploaded to Dropbox')? '<span style="color:green">'.$__trans['upl_to_dbx'].'</span>' : '<span style="color:red">'.$dropbox_statuses[$i].'</span>';
        $l .= '<li style="background:#F2F2F2; padding:4px 8px; margin:0 0 5px; display:block;">'.$file_link.' ('.boxit_filesize_format($file->size).') - <strong>'.$__trans['status'].'</strong> '.$upload_status.' | '.$dropbox_status.'</li>';
        $i ++;
    }
    $l .= '</ul>';


    //main email (to website owner)
    if($boxit_options['email_notification']) {
        $to = $boxit_options['notifications_address'];
        $subject = $boxit_options['main_template_subject'];
        $template = ($boxit_options['main_template'] != '')? apply_filters('the_content',$boxit_options['main_template']) : '';
        $message = boxit_email_filters($template, $l, $name_field, $email_field, $message_field, $folder);
        $headers = array (
           'From: '.get_bloginfo('name').' <'.get_bloginfo('admin_email').'>'
        );
        wp_mail($to, $subject, $message, $headers);
    }


    //optional email (to uploader (from email field or user email))
    if($boxit_options['extra_notification']) {
        $error = false;
        $to = $email_field; // if email field is enabled
        if($to == '') { //if email field is disabled - get current user email
            if(is_user_logged_in()) {
                $user = wp_get_current_user();
                $to = $user->user_email;
            } else {
                $error = true; //email field is disabled & user is not logged in - noone to notify
            }
        }
        //if current user is the one from main email notification (website owner) - don't send him the second email
        if($to == $boxit_options['notifications_address']) {
            $error = true;
        }
        if(!$error) {
            $subject = $boxit_options['extra_template_subject'];
            $template = ($boxit_options['extra_template'] != '')? apply_filters('the_content',$boxit_options['extra_template']) : '';
            $message = boxit_email_filters($template, $l, $name_field, $email_field, $message_field, $folder);
            $headers = array (
               'From: '.get_bloginfo('name').' <'.get_bloginfo('admin_email').'>'
            );
            wp_mail($to, $subject, $message, $headers);
        }
    }
    die();
}
add_action( 'wp_ajax_nopriv_boxit_email_notification', 'boxit_email_notification' );
add_action( 'wp_ajax_boxit_email_notification', 'boxit_email_notification' );





function boxit_email_filters($template='', $l='', $n='', $e='', $m='', $folder="") {
    $template = str_replace('{name}', $n, $template);
    $template = str_replace('{email}', $e, $template);
    $template = str_replace('{message}', $m, $template);
    $template = str_replace('{filelist}', $l, $template);
    $template = str_replace('{folder}', $folder, $template);
    return $template;
}




function boxit_filesize_format($size, $sizes = array('Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')) {
    if ($size == 0) return('n/a');
    return (round($size/pow(1000, ($i = floor(log($size, 1000)))), 2) . ' ' . $sizes[$i]);
}








