<?php



function emails_tab_fields() {
    $options = get_option('boxit_options');
    $fields = array(
        /* section */
        array(
            'id' => 'email_template',
            'title' => 'Main email template for upload notifications',
            'fields' => array(
                /* fields */
                array(
                    'id' => 'main_template',
                    'title' => '(Email will be sent to: '.$options['notifications_address'].')'
                ),
                array(
                    'id' => 'main_template_subject',
                    'title' => 'Email subject:'
                )
            )
        ),
        array(
            'id' => 'extra_email_template',
            'title' => 'Additional email template for upload notifications',
            'fields' => array(
                /* fields */
                array(
                    'id' => 'extra_template',
                    'title' => 'Sent to person who uploaded files.'
                ),
                array(
                    'id' => 'extra_template_subject',
                    'title' => 'Email subject:'
                )
            )
        )
    );
    return $fields;
}







function boxit_email_template_section() {
    echo '<input type="hidden" name="boxit_tab" value="emails"/>';
    ?>
    <p><strong>Available quick tags:</strong><br>
        <!--<strong>{folder}</strong> - Dropbox files destination (folder or path) <br>-->
        <strong>{name}</strong> - User name (if enabled) <br>
        <strong>{email}</strong> - User email (if enabled) <br>
        <strong>{message}</strong> - Custom upload message/comment (if enabled) <br>
        <strong>{filelist}</strong> - The list of uploaded files<br>
        <strong>{folder}</strong> - Folder to which files were uploaded
    </p>
    <?php
}

function boxit_email_template_main_template() {
    $options = get_option('boxit_options');
    ?>
    <style type="text/css">
        #wp-boxit_main_template-wrap { margin-left: -220px; max-width: 670px; }
    </style>
    <br>
    <?php
    wp_editor( stripslashes($options['main_template']), 'boxit_main_template', array('media_buttons'=>false, 'teeny'=>true) );
    ?>
    <?php
}

function boxit_email_template_main_template_subject() {
    $options = get_option('boxit_options');
    ?>
    <input name="boxit_options[main_template_subject]" type="text" id="main_template_subject"  value="<?php echo $options['main_template_subject'] ?>" />
    <br><br><br><br>
    <?php
}





function boxit_extra_email_template_section() {
    ?>
    <p>
        This email template accepts the same quick tags as the main email template above.
    </p>
    <?php
}

function boxit_extra_email_template_extra_template() {
    $options = get_option('boxit_options');
    ?>
    <style type="text/css">
        #wp-boxit_extra_template-wrap { margin-left: -220px; max-width: 670px; }
    </style>
    <br>
    <?php
    wp_editor( stripslashes($options['extra_template']), 'boxit_extra_template', array('media_buttons'=>false, 'teeny'=>true) );
    ?>
    <?php
}

function boxit_extra_email_template_extra_template_subject() {
    $options = get_option('boxit_options');
    ?>
    <input name="boxit_options[extra_template_subject]" type="text" id="extra_template_subject"  value="<?php echo $options['extra_template_subject'] ?>" />
    <?php
}