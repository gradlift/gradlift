<?php
/**
 * The default template for displaying IgnitionDeck projects.
 */

    $project_id = get_post_meta( $post->ID, 'ign_project_id', true );
    $project = new ID_Project( $project_id );
    $percent = get_post_meta( $post->ID, 'ign_percent_raised', true ) != '' ? get_post_meta( $post->ID, 'ign_percent_raised', true ) : 0;

    $colors = get_option( 'krown_colors' );
    
    $retina = krown_retina();
    
    $user_profile = get_site_url() . '/user/' . get_the_author_meta('user_nicename');
    //$user_profile = get_site_url() . '/user/' . get_the_author_meta('first_name') . '.' . get_the_author_meta('last_name');
    //$user_profile = get_site_url() . '/user/' . get_the_author_meta('user_login');
	
	/**
	 *  Get user from url
	 */
	 $user_info = get_user_meta( get_the_author_meta('ID') );
	 
	 
?>
<!-- content-igintion_product.php -->
    <article class="krown-id-item">

        <?php 
			//prar(get_metadata ( 'post', $post->ID ));
			//prar( $post );
			global $ultimatemember;
			//prar($user_info);	
			//prar( get_the_author_id() );
            if ( has_post_thumbnail( $post->ID ) ) {
                //$image = aq_resize( wp_get_attachment_url( get_post_thumbnail_id(), 'full' ), $retina === 'true' ? 510 : 255, null, false, false );
				$avatar_uri = wp_get_attachment_url( get_post_thumbnail_id(), 'full' );
               // $img_obj = '<a class="fancybox-thumb" href="' .  $user_profile . '"><figure class="img"><img src="' . $image[0] . '" width="' . $image[1] . '" height="' . $image[2] . '" alt="' . get_the_title() . '" /></figure><span></span></a>';
            } else if ( get_post_meta( $post->ID, 'ign_product_image1', true ) != '' ) {
                //$image = aq_resize( get_post_meta( $post->ID, 'ign_product_image1', true ), $retina === 'true' ? 510 : 255, null, false, false );
				$avatar_uri = get_post_meta( $post->ID, 'ign_product_image1', true );
                //$img_obj = '<a class="fancybox-thumb" href="' .  $user_profile . '"><figure class="img"><img src="' . $image[0] . '" width="' . $image[1] . '" height="' . $image[2] . '" alt="' . get_the_title() . '" /></figure><span></span></a>';
				
            //} elseif ( um_profile('profile_photo') ) {
				//$avatar_uri = um_get_avatar_uri( um_profile('profile_photo'), $attrs );
				//prar($avatar_uri);
				//$value = um_user('profile_photo', 260);
				//prar( $value);
				//echo $value;
			//} else {
				//$avatar_uri = um_get_default_avatar_uri();
			//}
			} else {
                //$img_obj = '';
				//global $ultimatemember;
				//prar( um_get_default_avatar_uri() );
				//Check if profile photo exists
				$uploadDir = wp_upload_dir();
				//prar($uploadDir);
				$profilePhotoFile = $uploadDir['basedir'] . '/ultimatemember/' . get_the_author_id() . '/' . 'profile_photo-190.jpg'; //prar($profilePhotoFile);
				$profilePhoto = $uploadDir['baseurl'] . '/ultimatemember/' . get_the_author_id() . '/' . 'profile_photo-190.jpg'; 
				if( file_exists( $profilePhotoFile ) ) {
					//prar( 'photo exists!') ;
					$avatar_uri = $profilePhoto;
				} else {
					$avatar_uri = um_get_default_avatar_uri() ;
				}
				//$image = aq_resize( um_get_default_avatar_uri(), $retina === 'true' ? 510 : 255, null, false, false );
				//prar($image);
				
            }
			//prar( $avatar_uri );
			$image = aq_resize( $avatar_uri, $retina === 'true' ? 510 : 255, null, false, false );
			$img_obj = '<a class="fancybox-thumb" href="' .  $user_profile . '"><figure class="img"><img src="' . $image[0] . '" width="' . $image[1] . '" height="' . $image[2] . '" alt="' . get_the_title() . '" /></figure><span></span></a>';

            if( isset ($img_obj) ) echo $img_obj;

        ?>
        <div class="container">

            <a href="<?php echo $user_profile ?>"><h3 class="title"><?php the_title(); ?></h3></a>

            <div class="cats"><?php krown_categories( $post->ID, 'project_category' ); ?></div>

            <section class="content"><?php echo get_post_meta( $post->ID, 'ign_project_description', true ); ?></section>

            <aside class="meta">

                <div class="krown-pie small" data-color="<?php echo ( intval( $percent ) > 99 ? $colors['pie3'] : $colors['pie2'] ); ?>"><div class="holder"><span class="value" data-percent="<?php echo $percent; ?>"><?php echo $percent; ?><sup>%</sup></span></div></div>

                <ul>
                    <li><span><?php echo apply_filters( 'id_funds_raised', getTotalProductFund( $project_id ), $project_id ) . '</span> ' . __( 'Pledged', 'krown' ); ?></li>
                    <!--<li><span><?php //echo $project->days_left() . '</span> ' . __( 'Days to go', 'krown' ); ?></li> -->
                </ul>

            </aside>

        </div>

    </article>