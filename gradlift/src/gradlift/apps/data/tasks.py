from __future__ import absolute_import

from celery import shared_task

from django import template
from django.template import loader
from django.core import mail
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static

from celery.utils.log import get_task_logger

from gradlift.apps import Constants

logger = get_task_logger('celery.tasks')

mail_connection = None


@shared_task(default_retry_delay=60, max_retries=3)
def send_mail(
        subject='', body='', from_email=settings.DEFAULT_FROM_EMAIL, to=None, bcc=None,
        attachments=None, headers=None, cc=None, fail_silently=False,
        html='', context={}, text_template=None, html_template=None, reply_to=None):
    """
    Send an email as a Celery task, optionally render bodies from templates.

    Accepts the same arguments as django.core.mail.EmailMessage from the
    function arguments or from the context.  When used as a delayed Celery task,
    all arguments must be JSON serializable.  For example, no ORM objects or
    real template context objects.

    """
    if text_template and body:
        raise ValueError("Cannot give both plain text 'body' and 'text_template' arguments")
    if html_template and html:
        raise ValueError("Cannot give both alternative 'html' and 'html_template' arguments")
    if not isinstance(context, template.Context):
        context = template.Context(context)
    if text_template:
        body = loader.get_template(text_template).render(context)
    if html_template:
        html = loader.get_template(html_template).render(context)

    # Support taking django.core.mail.EmailMessage args from the context
    subject = subject or context.get('subject', '')
    body = body or context.get('body', '')
    from_email = from_email or context.get('from_email', None)
    to = to or context.get('to', None)
    bcc = bcc or context.get('bcc', None)
    attachments = attachments or context.get('attachments', None)
    headers = headers or context.get('headers', None)
    cc = cc or context.get('cc', None)
    html = html or context.get('html', '')

    msg = mail.EmailMultiAlternatives(subject=subject, body=body, from_email=from_email, to=to, bcc=bcc,
                                      attachments=attachments, headers=headers, cc=cc, reply_to=reply_to)
    if html:
        msg.attach_alternative(html, "text/html")
    if not msg.recipients():
        raise ValueError("No recipients")

    # Re-use an SMTP connection if possible to avoid connection overhead
    global mail_connection
    if mail_connection is None:
        mail_connection = mail.get_connection()
    mail_connection.open()

    try:
        # send the email
        result = msg.send(fail_silently=fail_silently)
    except Exception, exc:
        logger.warning("Exception occurred while sending email (%s) to (%s). Retrying ...", subject, to)
        raise send_mail.retry(exc=exc)

    logger.info("Email (%s) was sent to (%s) successfully", subject, to)

    return result


@shared_task(default_retry_delay=60, max_retries=3)
def send_notification_email(comment_id):

    from gradlift.apps.data import models
    comment = models.Comment.objects.get(pk=comment_id)

    subject = "GradLift - Comment from {name}".format(name=comment.commenter.user.get_full_name())

    if comment.commenter.profile_picture:
        commenter_picture = settings.BASE_URL + comment.commenter.profile_picture.url
    else:
        commenter_picture = settings.BASE_URL + static('website/images/default_avatar.png')

    context = {
        'commentee_name': comment.commentee.user.get_full_name(),
        'commenter_name': comment.commenter.user.get_full_name(),
        'created_datetime': comment.created.strftime(Constants.FULL_DATETIME_DISPLAY),
        'profile_link': settings.BASE_URL + reverse('profile', args=(comment.commentee.link,)),
        'comment': comment.comment,
        'commenter_picture': commenter_picture
    }

    send_mail(
        subject=subject,
        to=(comment.commentee.user.email,),
        html_template='email_templates/comment_notification.html',
        context=context
    )

    comment.is_notification_sent = True
    comment.save()


@shared_task()
def send_notification_emails_daily():
    from gradlift.apps.data import models
    daily_comments = models.Comment.objects.filter(
        commentee__notification_frequency=models.UserProfile.NOTIFICATION_CHOICE_DAILY,
        is_notification_sent=False,
        is_removed=False
    )

    for daily_comment in daily_comments:
        send_notification_email.delay(comment_id=daily_comment.pk)


@shared_task()
def send_notification_emails_weekly():
    from gradlift.apps.data import models
    weekly_comments = models.Comment.objects.filter(
        commentee__notification_frequency=models.UserProfile.NOTIFICATION_CHOICE_WEEKLY,
        is_notification_sent=False,
        is_removed=False
    )

    for weekly_comment in weekly_comments:
        send_notification_email.delay(comment_id=weekly_comment.pk)


@shared_task()
def get_database_backups_daily():
    from django.core.management import call_command
    call_command('dbbackup')