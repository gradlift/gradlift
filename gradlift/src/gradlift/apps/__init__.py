import os
import zipfile
import StringIO

from django.http import HttpResponse


class Constants:

    def __init__(self):
        pass

    MONTH_YEAR_DATE_FORMAT = '%b - %Y'
    MONTH_YEAR_DISPLAY_DATE_FORMAT = "%B %Y"
    INDEX_PAGE_PROFILE_PREVIEWS_LIMIT = 100
    FULL_DATETIME_DISPLAY = "%B %d, %Y  %I:%M %P"
    FULL_DATE_DISPLAY = "%B %d, %Y"
    MONTH_DAY_YEAR_NORMAL_FORMAT = "%m/%d/%Y"

    PASSWORD_MIN_LENGTH = 6

    PAYMENT_OPTIONS = [25, 50, 75, 100, 250, 500, 999]

    DEFAULT_GOAL_AMOUNT = 1500.0

    MAX_NUMBER_OF_CARDS_PER_PAGE = 12

    EMAIL_SUBJECT_BACKER_RECEIPT = "GradLift - BACKER RECEIPT"
    EMAIL_SUBJECT_STUDENT_RECEIPT = "GradLift - STUDENT RECEIPT"
    EMAIL_SUBJECT_WELCOME = "GradLift - WELCOME!"

    CACHE_TIME_ONE_HOUR = 60 * 60
    CACHE_TIME_HALF_AN_HOUR = 30 * 60
    CACHE_TIME_10_MINUTES = 10 * 60
    CACHE_TIME_1_MIN = 60

    ANALYTICS_PARAM_REGISTER = 'step=register'
    ANALYTICS_PARAM_PROFILE_COMPLETED = 'step=profile-completed'


class Utility:

    def __init__(self):
        pass

    @staticmethod
    def zip_and_download_files(file_paths, zip_file_name):

        zip_subdir = zip_file_name
        zip_filename = "%s.zip" % zip_subdir

        # Open StringIO to grab in-memory ZIP contents
        in_memory_zip_file = StringIO.StringIO()

        # The zip compressor
        zip_file = zipfile.ZipFile(in_memory_zip_file, "w")

        for directory_name in file_paths:
            directory_file_paths = file_paths[directory_name]
            for fpath in directory_file_paths:
                # Calculate path for file in zip
                fdir, fname = os.path.split(fpath)
                zip_path = os.path.join(zip_file_name, directory_name, fname)

                # Add file, at correct path
                zip_file.write(fpath, zip_path)

        # Must close zip for all contents to be written
        zip_file.close()

        # Grab ZIP file from in-memory, make response with correct MIME-type
        resp = HttpResponse(in_memory_zip_file.getvalue(), content_type="application/x-zip-compressed")
        # ..and correct content-disposition
        resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

        return resp
