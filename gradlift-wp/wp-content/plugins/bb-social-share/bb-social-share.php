<?php
/*
Plugin Name: Bear Bones Social Share
Description: Social share ul>li>a with Font Awesome

 * NOTES: Based on the work by 
 * https://jonsuh.com/blog/social-share-links/
 * https://github.com/bradvin/social-share-urls
 
*/

//TODO: Customizer network options
//TODO: Clean up code
//TODO: Separate out class, widget & functions into different files

	
define( 'BBSS_PLUGIN_DIR', untrailingslashit( dirname( __FILE__ ) ) );
require_once BBSS_PLUGIN_DIR . '/includes/customizer.php';
require_once BBSS_PLUGIN_DIR . '/includes/widget.php';
require_once BBSS_PLUGIN_DIR . '/classes/class-bb-social-share.php';
require_once BBSS_PLUGIN_DIR . '/classes/class-bb-social-share-meta-box.php';

// Register and load the widget
function bb_social_share_load_widget() {
	register_widget( 'bb_social_share_widget' );
}
add_action( 'widgets_init', 'bb_social_share_load_widget' );



function bb_social_share( $atts = null ) {
	if( !isset( $atts['url'] ) ) {
		$id = isset( $atts['id'] ) ? $atts['id'] : null;
		$atts = bb_social_share_get_atts( $id );
	}
	if( is_array( $atts ) && isset( $atts['url'] ) ) {
	
		$title = ( isset( $atts['title'] ) ? $atts['title'] : null );
		$img = ( isset( $atts['img'] ) ? $atts['img'] : null );
		$desc = ( isset( $atts['desc'] ) ? $atts['desc'] : null );
		
		$bbss = new bbSocialShare;
		$bbss->bear_bones_social_share_list();
	}
}

function bb_social_share_get_atts ( $id = null ) {
	if( $id ) {
		$post = get_post( $id );
		
	} else {
		global $post;
		//$text = ($post->post_excerpt) ? $post->post_excerpt : get_the_content('');
    }
	$title = get_the_title( $post->ID ); 
	$url = get_the_permalink( $post->ID ); 
	$image = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
	
	if ( function_exists( 'bb_excerpt' ) ) {
		$excerpt = bb_excerpt( null, $post->ID, null );
	} else {
		$excerpt = get_the_excerpt( $post->ID );
	}
	
	$socialAtts = array(
		'url' => $url,
		'title' => $title,
		'img' => $image,
		'desc' => $excerpt,
		'other_vars' => array (
			'linkClass' => 'social-share__link js-social-share',
		),
		'networks' => array( 
			'facebook', 'twitter', 'pinterest', 'google', 'tumblr' 
		)
	);
	return $socialAtts;
}

function bb_social_share_add_to_content($content) {
	// Thanks to http://wordpress.stackexchange.com/questions/162747/the-content-and-is-main-query for in_the_loop!!!
	if( in_the_loop() ) {
		$display = false;
		if( is_single() ) {
			$display = get_theme_mod( 'bbss_show_on_posts' );
		} elseif ( is_page() ) {
			$display = get_theme_mod( 'bbss_show_on_pages' );
		}
		 if ( $display ) {
			//enqueue scripts
			bb_social_share_scripts();
			$before_content = get_theme_mod( 'bbss_show_before_content' );

			$atts = bb_social_share_get_atts();

			$bbss = new bbSocialShare;
			$socialShare = $bbss->bb_social_share_list( );

			if($before_content) {
				$content = $socialShare . $content;
			} else {
				$content .= $socialShare;
			}
		 }
	}
	return $content;
}

add_filter('the_content', 'bb_social_share_add_to_content');

/**
 *  Add social share event tracking for Google Analytics
 *  http://wordpress.stackexchange.com/questions/33008/how-to-add-a-javascript-snippet-to-the-footer-that-requires-jquery
 */


function bb_social_share_scripts() {
	wp_enqueue_script( 'sharify-counts', plugins_url( 'js/bb-social-share.js', __FILE__ ), array( 'jquery' ), null, true );
}

//Do not include on all pages, only include script when needed
//add_action( 'wp_enqueue_scripts', 'bb_social_share_scripts' );

//@TODO: MAYBE? Separate out specific options to separate plugin options and leave styling options in customizer, for example: Twitter Options on plugin options page: https://dev.twitter.com/web/tweet-button
?>