# 

class php {
    
     package { 'php-common':
                ensure		=> 'present',
                name		=> 'php-common',
        }

  package { 'php-cli':
                ensure		=> 'present',
                name		=> 'php-cli',
        }

  package { 'php':
                ensure		=> 'present',
                name		=> 'php',
        }

   package { 'autoconf':
                ensure		=> 'present',
                name		=> 'autoconf',
        }

   package { 'php-devel':
                ensure		=> 'present',
                name		=> 'php-devel',
        }

    package { 'php-gd':
                ensure		=> 'present',
                name		=> 'php-gd',
        }

     package { 'libc-client':
                ensure		=> 'present',
                name		=> 'libc-client',
        }

    package { 'php-imap':
                ensure		=> 'present',
                name		=> 'php-imap',
        }

     package { 'php-intl':
                ensure		=> 'present',
                name		=> 'php-intl',
        }

    package { 'php-mbstring':
                ensure		=> 'present',
                name		=> 'php-mbstring',
        }

     package { 'php-pdo':
                ensure		=> 'present',
                name		=> 'php-pdo',
        }

  package { 'php-mysql':
                ensure		=> 'present',
                name		=> 'php-mysql',
        }


 Package['php-common']->
    Package['php-cli']->
    Package['php']->
    Package['autoconf']->
    Package['php-devel']->
    Package['php-gd']->
    Package['libc-client']->
    Package['php-imap']->
    Package['php-intl']->
    Package['php-mbstring']->
    Package['php-pdo']->
    Package['php-mysql']


} 

