// ************ SOCIAL SHARE ************** //
(function($) {
	//https://yoast.com/easily-google-analytics-account-id/
	function returnGAcode() {
		if ( typeof ga !== 'undefined' ) {
			return "ga";
		} else if ( typeof _gaq !== 'undefined' ) {
			return "gaq";
		} else if ( typeof __gaTracker !== 'undefined' ) {
			return 'gat';
		} else {
			return false;
		}
	}
	
	//http://www.lunametrics.com/blog/2013/07/02/jquery-event-tracking-generator-google-analytics/
	$("a.bb-social-share__link").each(function() {
		$(this).click(function(event) { // when someone clicks these links
			var href = $(this).attr("href");
			var target = $(this).attr("target");
			var text = $(this).text();
			var network = $(this).data("social-network");
			//var shortlink = $('link[rel=shortlink]').attr('href');
			var title = $('h1.entry-title').text();
			event.preventDefault(); // don't open the link yet
			
			//Check to see what version of GA being used
			var GAcode = returnGAcode();
			
			if( GAcode == "gaq" ) {
				//if (window.console) console.log( "GA code: " + GAcode );
				//Check to make sure function exists
				//https://seo-hacker.com/google-analytics-tutorial-events-tracking/
				_gaq.push(['_trackEvent', 'Social Share', network, title ]);				
			} else if ( GAcode == "gat" ) {
				//if (window.console) console.log('GA code: ' + GAcode);
				__gaTracker('send', 'event', 'Social Share', network, title );
			} else if ( GAcode == "ga" ) {
				//if (window.console) console.log( "GA code: " + GAcode );
				ga('send', 'event', 'Social Share', network, title );
			} else {
				//alert('Check yourFunctionName!');
				if (window.console) console.log('GA not defined');
			}
			//Now open the link
			setTimeout(function() { // now wait 300 milliseconds...
				window.open(href,(!target?"_self":target)); // ...and open the link as usual
			},300);
		});
	});
})(jQuery);	