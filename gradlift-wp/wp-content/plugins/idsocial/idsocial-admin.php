<?php

add_action('admin_menu', 'idsocial_admin_menus', 11);

function idsocial_admin_menus() {
	$settings = add_submenu_page('idf', __('Social', 'idsocial'), __('Social', 'idsocial'), 'manage_options', 'idsocial', 'idsocial_menu');
	add_action('admin_print_styles-'.$settings, 'idf_admin_enqueues');
}

function idsocial_menu() {
	$settings = get_option('idsocial_settings');
	if (isset($_POST['submit_social_settings'])) {
		foreach ($_POST as $k=>$v) {
			$settings[$k] = $v;
		}
		update_option('idsocial_settings', $settings);
	}
	include IDSOCIAL_PATH.'templates/admin/_settingsMenu.php';
}
?>