
initial_data = {
    'auto_completes' : {
        'institutions' : [],
        'majors' : []
    }
};

$(function(){

    $('img.image-preview').hide();
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('img.image-preview').attr('src', e.target.result);
                $('img.image-preview').show();
                $('a.apply-image-upload').removeClass('disabled')
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    // Profile Picture section
   $("a#profile_picture_apply").on("click", function(){
       $("div.image-upload div.error").html("").css({'display': 'none'});
       $("#profile_picture_form").submit();
    });

    $("#profile_picture").change(function(){
        readURL(this);
    });
    $("#profile_picture_modal").hide();

    $("a#upload-profile-picture-button").click(function(e){
        $("#profile_picture_modal").show();
        $('.um-modal-overlay').show();
        $('.um-dropdown-hide').click();
    });

    $('.um-modal-overlay').click(function(e){
        hide_modal();
    });

    $("a.cancel-image-upload").click(function(e){
        hide_modal();
    });

    var hide_modal = function() {
        $('.um-modal-overlay').hide();
        $("#profile_picture_modal").hide();
        $(".image-upload").hide();
    };

    // Cover Image section
   $("a#cover_image_apply").on("click", function(){
       $("div.image-upload div.error").html("").css({'display': 'none'});
       $("#cover_image_form").submit();
    });

    $("#cover_image").change(function(){
        readURL(this);
    });
    $("#cover_image_modal").hide();

    $("a#upload-cover-image-button").click(function(e){
        $("#cover_image_modal").show();
        $('.um-modal-overlay').show();
        $('.um-dropdown-hide').click();
    });


    setup_cover_image_image();

    setup_profile_previews_layouts();

    $("form.comment-delete-form").submit(
        function(event){

            if (!confirm("Are you sure you want to remove your comment?")) {
                event.preventDefault();
            }

        }
    );

});

var setup_cover_image_image = function() {
    if ($("#cover-image-image").attr("src")) {
        $("#upload-cover-image-button").addClass("cover-image-button");
        $("#cover-image-image").show();
    } else {
        $("#cover-image-image").hide();
    }
};

var profile_ajax_functions = {
    on_profile_picture_success: function(data) {
        if (data && data.success == true) {
            $("img#profile-picture-image").attr('src', data.new_image_url);
            $("a.cancel-image-upload").click();
            $(".image-upload").hide();
        }
    },
    on_profile_picture_error: function(data) {

        var errors = data.responseJSON['profile_picture'];
        $("div.image-upload div.error").html(errors).css({'display': 'block'});
    },

    on_cover_image_success: function(data) {
        if (data && data.success == true) {
            $("img#cover-image-image").attr('src', data.new_image_url);
            $("a.cancel-image-upload").click();
            $(".image-upload").hide();
        }
        setup_cover_image_image();
    },
    on_cover_image_error: function(data) {

        var errors = data.responseJSON['cover_image'];
        $("div.image-upload div.error").html(errors).css({'display': 'block'});
    }
};



// When the document is ready
$(document).ready(function () {

    $("#id_birthday").datepicker({
        startView: 2,
        autoclose: true
    });

    add_month_year_calendar('#id_high_school_graduation_date');

    setup_school_section("#high_school_section");

    setup_remove_college_section();

    setup_autocomplete(initial_data.auto_completes.institutions, 'institutions', '#id_high_school_name');


    // Setup College 1 front-end
    setup_autocomplete(initial_data.auto_completes.institutions, 'institutions', '#id_college_1_name');

    setup_autocomplete(initial_data.auto_completes.majors, 'majors', '#id_college_1_major');

    add_month_year_calendar('#id_college_1_start_date');

    add_month_year_calendar('#id_college_1_end_date');


    // Setup College 2 font-end
    setup_add_college_section("#add_another_college");


    setup_autocomplete(initial_data.auto_completes.institutions, 'institutions', '#id_college_2_name');

    setup_autocomplete(initial_data.auto_completes.majors, 'majors', '#id_college_2_major');

    add_month_year_calendar('#id_college_2_start_date');

    add_month_year_calendar('#id_college_2_end_date');


    setup_autocomplete(initial_data.auto_completes.institutions, 'institutions', '#id_college_3_name');

    setup_autocomplete(initial_data.auto_completes.majors, 'majors', '#id_college_3_major');

    add_month_year_calendar('#id_college_3_start_date');

    add_month_year_calendar('#id_college_3_end_date');


    setup_autocomplete(initial_data.auto_completes.institutions, 'institutions', '#id_college_4_name');

    setup_autocomplete(initial_data.auto_completes.majors, 'majors', '#id_college_4_major');

    add_month_year_calendar('#id_college_4_start_date');

    add_month_year_calendar('#id_college_4_end_date');

    /*
    $("#testtest").click(function(){
        http.post("upload-profile-picture/", {}, function(success) {
            alert("Authorized");
        });
    });
    */

});


var setup_school_section = function(section){

    var high_school_section = $(section);
    var value = parseInt($('input[type=radio][name=is_high_school_student]:checked').val());

    if (value == 1) {
        high_school_section.collapse('show');
    } else {
        high_school_section.addClass('collapse');
    }

    $('input[type=radio][name=is_high_school_student]').change(function() {
        if (this.value == 1) {
            high_school_section.collapse('show');
        } else {
            high_school_section.collapse('hide');
        }
    });
};

var has_value_1 = function(element) {
     return parseInt($(element).val()) == 1;
};

var setup_add_college_section = function(trigger) {

    var new_college_number = 2;

    if (has_value_1('#id_attends_college_2')) {
        $('#college_2_section').collapse('show');
        new_college_number = 3;
    } else {
        $('#college_2_section').addClass('collapse');
    }

    if (has_value_1('#id_attends_college_3')) {
        $('#college_3_section').collapse('show');
        new_college_number = 4;
    } else {
        $('#college_3_section').addClass('collapse');
    }

    if (has_value_1('#id_attends_college_4')) {
        $('#college_4_section').collapse('show');
        $('#add_another_college_section').hide();
    } else {
        $('#college_4_section').addClass('collapse');
    }

    $('#add_another_college').attr("data-college-number-top-open", new_college_number);

    $(trigger).click(function (event) {
        add_college_click(event);
    });

};


var add_college_click = function(event) {

    event.preventDefault();
    var college_number = $('#add_another_college').attr("data-college-number-top-open");
    var section_selector = '#college_' + college_number + '_section';
    var hidden_field = '#id_attends_college_' + college_number;
    $(hidden_field).val(1);

    var section = $(section_selector);

    var new_college_number = parseInt(college_number) + 1;
    if (new_college_number >= 5) {
        $('#add_another_college_section').hide();
    } else {
        $('#add_another_college').attr("data-college-number-top-open", new_college_number)
    }

    section.collapse('show');

};

var setup_remove_college_section = function() {

    $(".college-remove-button").click(function(event){
        event.preventDefault();
        var college_number = $(this).attr('data-college-number');
        $("#college_" + college_number + "_section").collapse('hide');
        $("#id_attends_college_" + college_number).val(0);
        $('#add_another_college').attr("data-college-number-top-open", college_number);
        $('#add_another_college_section').show();
    });

};

var add_month_year_calendar = function(input) {
    $(input).datepicker({
        format: "M - yyyy",
        startView: "months",
        minViewMode: "months",
        autoclose: true
    });
};

var setup_autocomplete = function(source, name, input) {
    var substringMatcher = function(strs) {
      return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            matches.push(str);
          }
        });

        cb(matches);
      };
    };

    $(input).typeahead({
      hint: false,
      highlight: true,
      minLength: 2
    },
    {
      name: name,
      source: substringMatcher(source),
      limit: 100
    });
};


/**
 * Pledge progress bar
 */
var setup_pledges_ui = function() {
    $(".pledge-progress:not(.pledge-drawn)").each(function(i, topLoader) {

        var $topLoader = $(topLoader);

        var percentage = parseFloat($topLoader.attr("percentage"))/100.0;
        var currentAmount = parseFloat($topLoader.attr("current-amount"));
        var goalAmount = parseFloat($topLoader.attr("goal-amount"));
        var backers = parseInt($topLoader.attr("backers"));

        $topLoader.percentageLoader({
            width: 150,
            height: 150,
            controllable : false,
            progress : 0.5
        });

        $topLoader.setProgress(0, true, percentage);
        $topLoader.setValue(currentAmount);
        $topLoader.setGoalAmount(goalAmount);
        $topLoader.setBackers(backers);

        $topLoader.drawLoader();
        $topLoader.addClass('pledge-drawn');

    });

    $("#id_message").prop('required',true);

};

$(setup_pledges_ui);

var setup_profile_previews_layouts = function() {
    var preview_container_ids = [
        '#discover-previews-section',
        '#featured-gradline-section',
        '#recently-added-section',
        '#high-school-section',
        '#college-section',
        '#grad-school-section'
    ];

    preview_container_ids.forEach(function(preview_container_id) {
        setup_profile_previews_layout(preview_container_id);
    });
};

var setup_profile_previews_layout = function(preview_container_id) {

    var browserWidth = $(window).width();

    var cards = [];
    $(preview_container_id+".profile-cards .profile-overview").each(function(index, card){
        cards.push(card);
    });

    $(preview_container_id+".profile-cards .profile-overview, "+preview_container_id+".profile-cards .profile-cards-column").remove();

    var numberOfColumns = 1;
    if (browserWidth >= 980) {
        numberOfColumns = 4;
    } else if (browserWidth >= 780) {
        numberOfColumns = 3;
    } else if (browserWidth >= 580) {
        numberOfColumns = 2;
    }

    var columns = [];
    for (var i=0; i<numberOfColumns; i++) {
        var column = document.createElement('div');
        $(column).addClass('col-xs-3 profile-cards-column');
        columns.push(column);
        $(column).appendTo($(preview_container_id+".profile-cards"));
    }

    var j=0;
    cards.forEach(function(card){
        var columnIndex = j%numberOfColumns;
        $(card).appendTo($(columns[columnIndex]));
        j++;
    });

};