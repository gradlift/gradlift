from django.core.management.base import BaseCommand, CommandError
from gradlift.apps.data import models
import csv
import re


class Command(BaseCommand):
    help = 'Imports Majors'

    def add_arguments(self, parser):
        parser.add_argument('file', type=str)

    def handle(self, *args, **options):
        path = options['file']
        with open(path) as f:
            reader = csv.reader(f)

            for row in reader:

                major_name = row[1].title().strip()
                major_code = major_name.upper().replace(" ", "_").replace(",", "")

                try:
                    models.RefMajor.objects.get(code=major_code)
                except models.RefMajor.DoesNotExist:
                    major = models.RefMajor(name=major_name, code=major_code)
                    major.save()