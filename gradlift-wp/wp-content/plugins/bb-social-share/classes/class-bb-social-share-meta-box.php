<?php

/**
 * Calls the class on the post edit screen.
 */
function call_bbSocialShareMetaBox() {
    new call_bbSocialShareMetaBox();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'call_bbSocialShareMetaBox' );
    add_action( 'load-post-new.php', 'call_bbSocialShareMetaBox' );
}

/** 
 * The Class.
 */
class call_bbSocialShareMetaBox {
	
	
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save' ) );
	}

	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
            $post_types = array('post', 'page');     //limit meta box to certain post types
            if ( in_array( $post_type, $post_types )) {
		add_meta_box(
			'bb_social_share_meta_box'
			,__( 'Social Share', 'bb_social_share' )
			,array( $this, 'render_meta_box_content' )
			,$post_type
			,'normal'
			,'high'
		);
            }
	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
	
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['bb_social_share_inner_custom_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['bb_social_share_inner_custom_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'bb_social_share_inner_custom_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
                //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;
	
		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */

		// Sanitize the user input.
		$twitterHashtags = sanitize_text_field( $_POST['bb_social_share_twitter_hashtags'] );
		$useShortlink = ( isset( $_POST['bb_social_share_twitter_shorturl'] ) && true == $_POST['bb_social_share_twitter_shorturl'] ? true : false );

		// Update the meta field.
		update_post_meta( $post_id, 'bb_social_share_twitter_hashtags', $twitterHashtags );
		update_post_meta( $post_id, 'bb_social_share_twitter_shorturl', $twitterHashtags );
	}


	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {
	
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'bb_social_share_inner_custom_box', 'bb_social_share_inner_custom_box_nonce' );

		// Use get_post_meta to retrieve an existing value from the database.
		$hashtags = esc_attr( get_post_meta( $post->ID, 'bb_social_share_twitter_hashtags', true ) );
		$shorturl = ( esc_attr( get_post_meta( $post->ID, 'bb_social_share_twitter_shorturl', true ) ) ? 'checked' : null );
		//prar("Shortlink: $shorturl");
		
		// Display the form, using the current value.
		?>
		<p class="bb-social-share-admin__title">Twitter</p>
		<label for="bb_social_share_twitter_hashtags">
		<?php _e( 'Hashtags (comma separated, no #, no spaces):', 'bb_social_share' ); ?>
		</label>
		<input type="text" id="bb_social_share_twitter_hashtags" name="bb_social_share_twitter_hashtags" value="<?php echo $hashtags ; ?>" size="45" />
		<p><a href="https://dev.twitter.com/web/tweet-button">Twitter dev reference</a></p>
		<label for="bb_social_share_twitter_shorturl"><?php _e( 'Use shortlink', 'bb_social_share' ); ?></label>
		<input type="checkbox" name="bb_social_share_twitter_shorturl" id="bb_social_share_twitter_shorturl" <?php echo $shorturl; ?>>
		<?php
		
	}
}
?>