#
# Installs packages needed for python development.
#
class python () {
    # Python 2.7 environment
    yumrepo { "scl_python27":
        descr => "Python 2.7 Dynamic Software Collection",
        baseurl => "http://people.redhat.com/bkabrda/python27-rhel-6/",
        failovermethod => "priority",
        enabled => 1,
        gpgcheck => 0,
        http_caching => all,
    }
    package { "python27-python":
        ensure => present,
        require => Yumrepo["scl_python27"],
    }
    package { "python27-python-devel":
        ensure => present,
        require => Yumrepo["scl_python27"],
    }
    package { "python27-python-setuptools":
        ensure => present,
        require => Yumrepo["scl_python27"],
    }
    package { "python27-python-virtualenv":
        ensure => present,
        require => Yumrepo["scl_python27"],
    }
    package { "libjpeg-turbo-devel":
        ensure => present,
        require => Yumrepo["scl_python27"],
    }
    package { "libzip-devel":
        ensure => present,
        require => Yumrepo["scl_python27"],
    }
    package { "libtiff-devel":
        ensure => present,
        require => Yumrepo["scl_python27"],
    }
    package { "freetype-devel":
        ensure => present,
        require => Yumrepo["scl_python27"],
    }
    package { "tcl-devel":
        ensure => present,
        require => Yumrepo["scl_python27"],
    }
    package { "tk-devel":
        ensure => present,
        require => Yumrepo["scl_python27"],
    }
}

#
# System-wide support for hosting API Django instances.
#
# Parameters:
# - The $unix_user that owns the source of the project.
# - The $eggs directory for Python distributions shared between instances.
# - The $downlods directory for downloads shared between instances.
#
class django () {
    # Base Python environment
    require python

    $rubygemsPackages = [ 'ruby-devel', 'rubygems' ]

    # We need ruby gems to install Frontend packages
    package { $rubygemsPackages:
         ensure => present,
    }

    $frontendPackages = [ 'bundle' ]

    # Ruby gems is required for running Frontend tasks like node, compass or grunt
    package { $frontendPackages:
      ensure => 'installed',
      provider => 'gem',
    }

    # The MySQL client is required for some custom commands and for
    # building instances
    package { "mysql":
        ensure => present,
    }
    # Required by the MySQL Python extension module
    package { "mysql-devel":
        ensure => present,
    }

    file { ["/var/log/django/error.log"]:
        ensure => file,
        owner  => "vagrant",
        group  => "vagrant",
    }
}

