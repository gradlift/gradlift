from django.core.management.base import BaseCommand, CommandError
from gradlift.apps.data import models
import csv
import re


class Command(BaseCommand):
    help = 'Imports Institutions'

    def add_arguments(self, parser):
        parser.add_argument('file', type=str)

    def handle(self, *args, **options):
        path = options['file']
        with open(path) as f:
            reader = csv.reader(f)
            country = models.RefCountry.objects.get(code='US')
            for row in reader:

                state = models.RefState.objects.get(code=row[4])

                institution = models.Institution(unit_id=row[0],
                                                 name=re.sub(r'[^\x00-\x7F]+', ' ', row[1]),
                                                 phone_number=row[10],
                                                 fax_number=row[11],
                                                 website=row[15],
                                                 zip_code=row[5],
                                                 street=re.sub(r'[^\x00-\x7F]+', ' ', row[2]),
                                                 city=re.sub(r'[^\x00-\x7F]+', ' ', row[3]),
                                                 state=state,
                                                 country=country
                                                 )

                institution.save()