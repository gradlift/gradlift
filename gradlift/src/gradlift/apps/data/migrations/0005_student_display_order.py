# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0004_comment_is_removed'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='display_order',
            field=models.PositiveIntegerField(default=None, null=True, blank=True),
        ),
    ]
