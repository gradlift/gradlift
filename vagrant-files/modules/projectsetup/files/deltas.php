<?php
// skipping a range of deltas? use these variables.
// 0's in either variable disable the skip feature
$minSkip = 0;
$maxSkip = 0;
$deltaDir = '/vagrant/gradlift-wp/db/';
$dbHost = 'localhost';
$dbUser = 'gradliftwp';
$dbPass = '123456';
$dbCore = 'gradliftwp';

// Quick and dirty script to run deltas
$db = mysql_connect($dbHost, $dbUser, $dbPass);

$schemaHistory = $deltaDir.'01.sql';
echo "Processing: ".$schemaHistory . "\n";

if (file_exists($schemaHistory)) {
    $command = "mysql --user=$dbUser --password=$dbPass --database=$dbCore < " . $schemaHistory;
    exec($command);
} else {
    exit('Delta 01.sql not found');
}

mysql_close($db);