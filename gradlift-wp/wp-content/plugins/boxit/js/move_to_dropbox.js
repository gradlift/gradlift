/***************************************************/
/*                                                 */
/*    Move uploaded fiel to Dropbox (AJAX call)    */
/*                                                 */
/***************************************************/
function move_to_dropbox(file, status) {
    jQuery(document).ready(function( $ ) {
        var debug = $('#boxit').attr('data-debug');
        if(debug) console.log('... sending file '+file.name+' to Dropbox ...');

        //on plupload error...
        if(!status) {
            $('#'+file.id).append('<div class="dbx_error boxit_text_1 plupload_error">c</div><div class="dbx_error_message plupload_error_message" style="color:red; position:relative; z-index:9999;">'+__trans.server_upl_failed+'</div>');
            //if its the last file - sent the status email
            maybe_upload_email();
            //debug info
            if(debug) console.log('BOXIT AJAX: plupload error passed to Dropbox sender. Aborting Dropbox upload for file '+file.name);
            return;
        }

        //append "moving to dropbox" icon to the file <li>
        $('#'+file.id).prepend('<div class="moving_to_dbx boxit_text_1">l</div>');

        $.ajax({
            type: 'POST',
            url: boxit_ajax.ajaxurl,
            data: {
                action: 'move_to_dropbox',
                file_id: file.id,
                file_name: file.name,
                name_input: $('#boxit_name_field').val(),
                shortcode_folder: $('#boxit').attr('data-shortcode_folder'),
                organize: $('#boxit').attr('data-organize')
            },
            success: function(data, textStatus, jqXHR) {
                $('#'+file.id).addClass('dbx_upload_done');
                //Recive the result of dropbox upload. If its a json string (containing upload data) upload was succesfull, else error message is returned
                if(is_json(data)) {
                    var file_data = JSON.parse(data); //turn returned file data to object
                    //add Dropbox SUCCESS result to the file item <li>
                    $('#'+file.id).attr('data-path', file_data.path);
                    $('#boxit').attr('data-folder', file_data.folder);
                    $('#'+file.id).append('<div class="dbx_success boxit_text_1">s</div>');
                    if(debug) console.log('BOXIT AJAX: Upload to Dropbox success! (file '+file.name+')');
                    if(debug) console.log('-- file data: '+file_data);
                } else {
                    //add Dropbox ERROR result to the file item <li>
                    $('#'+file.id).append('<div class="dbx_error boxit_text_1">c</div><div class="dbx_error_message">'+data+'</div>');
                    if(debug) console.log('BOXIT AJAX: Upload to Dropbox error (file '+file.name+'):'+data);
                }
                $('#'+file.id).removeClass('boxit_syncing');
                if($('#filelist .moving_to_dbx:visible').length == 0) $('#queue_header').find('div').remove();
                //if its the last file - sent the status email
                maybe_upload_email();
                boxit_space_status();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if(debug) {
                    console.log('--- "MOVE TO DROPBOX" AJAX ERROR ---');
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                    if (jqXHR.status === 0) {
                        console.log('Not connected. Verify Network.');
                    } else if (jqXHR.status == 404) {
                        console.log('Requested page not found. [404]');
                    } else if (jqXHR.status == 500) {
                        console.log('Internal Server Error [500].');
                    } else if (exception === 'parsererror') {
                        console.log('Requested JSON parse failed.');
                    } else if (exception === 'timeout') {
                        console.log('Time out error.');
                    } else if (exception === 'abort') {
                        console.log('Ajax request aborted.');
                    } else {
                        console.log('Uncaught Error. ' + jqXHR.responseText);
                    }
                    console.log('--- END OF "MOVE TO DROPBOX" AJAX ERROR REPORT ---');
                }
                $('#'+file.id).addClass('dbx_upload_done');
                $('#'+file.id).removeClass('boxit_syncing');
                $('#'+file.id).append('<div class="dbx_error boxit_text_1">c</div><div class="dbx_error_message">'+__trans.mtd_ajax_err+'</div>');
                if($('#filelist .moving_to_dbx:visible').length == 0) $('#queue_header').find('div').remove();
                //if its the last file - sent the status email
                maybe_upload_email();
            }
        });
    });
}





function maybe_upload_email() {
    jQuery(document).ready(function( $ ) {
        var debug = $('#boxit').attr('data-debug');
        /**
         * Trigger email confirmation ?
         * 1. Check if the file that just got uploaded to the server is the last one in queue
         *    (after last file is procesed the data-files attr. will be available on #boxit wrapper)
         *    this only tells us that all files were uploaded to the server (not dropbox)
         * 2. Check if all files finished uploading to dropbox
         *    check if in file queue <ul> are any "moving to dropbox" icons that are visible (that are still being moved to dropbox)
         *    if there is no visible "moving to dropbox" icons consider all files moved to dropbox
         */
        var dropbox_statuses = [], //dropbox upload statuses array - filled after all files finish uploading to dropbox (or got the upload error)
            paths = [], // - required for creating filelinks in email notif. - filled after all files finish uploading to dropbox either with data-path attr from file <li> (on success) or '0' on upload error
            files_attr = $('#boxit').attr('data-files'),
            dropbox_loaders_length = $('.moving_to_dbx:visible').length,
            name_field = $('#boxit_name_field').val(), // "undefined" if field  not available
            email_field = $('#boxit_email_field').val(), // "undefined" if field  not available
            message_field = $('#upload_message_field').val(), // "undefined" if field  not available
            folder = $('#boxit').attr('data-folder');

            if(name_field == undefined) name_field = 0;
            if(email_field == undefined) email_field = 0;
            if(message_field == undefined) message_field = 0;
            if(folder == undefined) folder = 0;

        if(files_attr && dropbox_loaders_length == 0) {
            //fill the "dropbox_statuses" array with dropbox upload results for each file in the queue
            $('#filelist li').each(function() {
                var li = $(this);
                var file_path = (li.attr('data-path') != undefined)? li.attr('data-path') : 0;
                if(li.find('.dbx_success').length > 0) {
                    dropbox_statuses.push('uploaded to Dropbox');
                    paths.push(file_path);
                } else {
                    dropbox_statuses.push(li.find('.dbx_error_message').html());
                    paths.push(file_path);
                }
            });
            //debug info
            if(debug) {
                console.log('------------------------------');
                console.log('ALL FILES IN QUEUE PROCESSED');
                console.log('------------------------------');
                console.log('Files from the queue:');
                console.log(JSON.parse(files_attr));
                console.log('------------------------------');
                console.log('Dropbox folder: '+folder);
                console.log('------------------------------');
                console.log('File paths:');
                console.log(paths);
                console.log('------------------------------');
                console.log('File from the queue statuses:');
                console.log(dropbox_statuses);
                if(name_field) console.log('Name field: '+name_field);
                if(email_field) console.log('Email field: '+email_field);
                if(message_field) console.log('Message field: '+message_field);
            }

            //trigger email notify
            var sending_email = false;
            if($('#boxit').attr('data-email_notification') == 1) {
                /**
                 * Send email
                 */
                sending_email = true;
                $.ajax({
                    type: 'POST',
                    url: boxit_ajax.ajaxurl,
                    data: {
                        action: 'boxit_email_notification',
                        files: files_attr,
                        dropbox_statuses: dropbox_statuses,
                        name_field: name_field,
                        email_field: email_field,
                        message_field: message_field,
                        folder: folder,
                        paths: paths
                    },
                    success: function(data, textStatus, jqXHR) {
                        $('.boxit_start_upload_container').after(data);
                        sending_email = false;
                        //redirect to thank you page - if ebnabled
                        if($('#boxit').attr('data-thankyou_page') != '' && sending_email == false) {
                            setTimeout(function(){
                                window.location.href = $('#boxit').attr('data-thankyou_page');
                            }, 2000);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('--- BOXIT EMAIL NOTIFICATION AJAX ERROR ---');
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                        if (jqXHR.status === 0) {
                            console.log('Not connected. Verify Network.');
                        } else if (jqXHR.status == 404) {
                            console.log('Requested page not found. [404]');
                        } else if (jqXHR.status == 500) {
                            console.log('Internal Server Error [500].');
                        } else if (exception === 'parsererror') {
                            console.log('Requested JSON parse failed.');
                        } else if (exception === 'timeout') {
                            console.log('Time out error.');
                        } else if (exception === 'abort') {
                            console.log('Ajax request aborted.');
                        } else {
                            console.log('Uncaught Error. ' + jqXHR.responseText);
                        }
                        console.log('--- END OF BOXIT EMAIL NOTIFICATION AJAX ERROR ---');
                        sending_email = false;
                        //redirect to thank you page - if ebnabled
                        if($('#boxit').attr('data-thankyou_page') != '' && sending_email == false) {
                            setTimeout(function(){
                                window.location.href = $('#boxit').attr('data-thankyou_page');
                            }, 2000);
                        }
                    }
                });
            }
            /**
             * Show "THANK YOU" message
             */
            if($('#boxit').attr('data-thank_you') == '1') {
                if(__trans.all_done == undefined) __trans.all_done = 'All files uploaded';
                $('.done_msg').remove();
                $('#boxit_dropzone, #queue_header, #boxit_start_upload').remove();
                $('#filelist').hide();
                $('#boxit').append('<div id="boxit_done_msg" class="boxit_text_1 boxit_bgd_1">'+__trans.all_done+'</div>');
            }
            /**
             * redirect to thank you page                                       [description]
             */
            if($('#boxit').attr('data-thankyou_page') != '' && sending_email == false) {
                setTimeout(function(){
                    window.location.href = $('#boxit').attr('data-thankyou_page');
                }, 2000);
            }

        }
    });
}




function is_json(jsonString){
    try {
        var o = JSON.parse(jsonString);

        if (o && typeof o === "object" && o !== null) {
            return true;
        }
    }
    catch (e) { }

    return false;
};







