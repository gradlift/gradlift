mysql --user=root --password='' -e "GRANT USAGE ON *.* TO 'gradlift'@'localhost';"
mysql --user=root --password='' -e "DROP USER 'gradlift'@'localhost';"
mysql --user=root --password='' -e "CREATE USER 'gradlift'@'localhost';"
mysql --user=root --password='' -e "GRANT ALL PRIVILEGES ON *.* TO 'gradlift'@'localhost' WITH GRANT OPTION;"
mysql --user=root --password='' -e "UPDATE mysql.user SET Password=PASSWORD('123456') WHERE User='gradlift';"
mysql --user=root --password='' -e "FLUSH PRIVILEGES;"
mysql --user=root --password='' -e "grant all on *.* to gradlift@'%' identified by '123456';"
mysql --user=root --password='' -e "DROP DATABASE IF EXISTS gradlift; CREATE DATABASE gradlift DEFAULT CHARACTER SET utf8; USE gradlift;"
echo 'db.gradlift.user' > /root/db.gradlift.user
