<?php

require_once um_social_login_path . 'providers/facebook/api/autoload.php';

class UM_Social_Login_Facebook {

	function __construct() {
		
		add_action('init', array(&$this, 'load'));
		
		add_action('init', array(&$this, 'get_auth'));

	}

	/***
	***	@load
	***/
	function load() {

		$app_id = ( um_get_option('facebook_app_id') ) ? um_get_option('facebook_app_id') : 'APP_ID';
		$app_secret = ( um_get_option('facebook_app_secret') ) ? um_get_option('facebook_app_secret') : 'APP_SECRET';
		
		$this->app_id             	= $app_id;
		$this->app_secret         	= $app_secret;
		$this->required_scope     	= 'public_profile, email';
		$this->redirect_url 		= trailingslashit( get_bloginfo('url') );
		$this->redirect_url 		= add_query_arg('facebook_auth', 'true', $this->redirect_url);

		$this->login_url		  	= '';

	}

	/***
	***	@Get auth
	***/
	function get_auth() {
		global $um_social_login;
		
		if ( isset( $_REQUEST['facebook_auth'] ) && $_REQUEST['facebook_auth'] == 'true' ) {
			
			// Initialize the Facebook PHP SDK v5.
			$fb = new Facebook\Facebook([
			  'app_id'                => $this->app_id,
			  'app_secret'            => $this->app_secret,
			  'default_graph_version' => 'v2.2',
			]);
			
			$helper = $fb->getRedirectLoginHelper();
			try {
			  $accessToken = $helper->getAccessToken();
			} catch(Facebook\Exceptions\FacebookResponseException $e) {

			} catch(Facebook\Exceptions\FacebookSDKException $e) {

			}
			
			if ( !isset( $accessToken ) ){
				$accessToken = $_SESSION['facebook_access_token'];
			}
			
			if (isset($accessToken)) {
				$_SESSION['facebook_access_token'] = (string) $accessToken;
				$fb->setDefaultAccessToken( $accessToken );
				$res = $fb->get('/me?fields=id,name,email,link');
				$profile = $res->getGraphObject()->asArray();
				
				if ( isset( $profile['name'] ) && $profile['name'] ) {
					$name = $profile['name'];
					$name = explode(' ', $name);
					$profile['first_name'] = $name[0];
					$profile['last_name'] = $name[1];
				}

				// prepare the array that will be sent
				$profile['user_email'] = $profile['email'];

				// username/email exists
				$profile['email_exists'] = $profile['email'];
				$profile['username_exists'] = $profile['email'];
				
				// provider identifier
				$profile['_uid_facebook'] = $profile['id'];
				
				$profile['_save_synced_profile_photo'] = 'http://graph.facebook.com/'.$profile['id'].'/picture?width=200&height=200';
				$profile['_save_facebook_handle'] = $profile['name'];
				$profile['_save_facebook_link'] = $profile['link'];

				// have everything we need?
				$um_social_login->resume_registration( $profile, 'facebook' );

			}

		}
		
	}
		
	/***
	***	@get login uri
	***/
	function login_url() {
		global $ultimatemember;

		$fb = new Facebook\Facebook([
			  'app_id'                => $this->app_id,
			  'app_secret'            => $this->app_secret,
			  'default_graph_version' => 'v2.2',
		]);
		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email']; // optional
		$callback = $this->redirect_url;
		$this->login_url = $helper->getLoginUrl($callback, $permissions);
		
		return $this->login_url;
		
	}
		
}