# -*- coding: utf-8 -*-
import json
import logging
import random

from datetime import datetime
from django.conf import settings as gradlift_settings
from django.http import HttpRequest

from gradlift.apps.data import models


logger = logging.getLogger(__name__)


def settings(request):
    """
    Specific settings that we might need to access from the templates.

    motivation - the 4 goal statements for the current user
    slogan - the Slogan for the current user
    """

    # skip if this is a call for any APIs or tasks
    if request.path.startswith('/api/') or len(request.path) > 1 and request.path[1].isdigit() or not isinstance(
            request, HttpRequest):
        return {}

    # add common settings for journey and staff
    context = {
        'current_year': datetime.now().year,
        'TITLE': "title",
        'IS_PRODUCTION': "",
        'BASE_URL': gradlift_settings.BASE_URL,
        'contribution_reference_data': models.RefContributionType.objects.get_reference_data(),
        'STRIPE_PUBLISHABLE': gradlift_settings.STRIPE_PUBLISHABLE,
        'IS_ANALYTICS_ENABLED': gradlift_settings.IS_ANALYTICS_ENABLED,
        'IS_PAYMENT_RECURRING_ENABLED': gradlift_settings.IS_PAYMENT_RECURRING_ENABLED,
        'IS_REFERRAL_CODE_ENABLED': gradlift_settings.IS_REFERRAL_CODE_ENABLED
    }

    return context