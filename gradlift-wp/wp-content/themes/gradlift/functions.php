<?php

/* This file should contain all the functions of the child theme. Most of the theme's functions can be overwritten (some are critical and shouldn't be tampered with). 

--- Below you have a full list of the ones which you can look up in the parent theme's functions.php file and redeclare here:

- krown_retina (to disable retina images by setting the cookie always to False)
- krown_setup (to add things at theme setup - should be modified with care)
- krown_analytics
- krown_filter_wp_title (to change how the theme's title appears)
- krown_excerptlength_post (to change the lenght of the post's excerpt in carousel shortcode)
- krown_excerptlength_post_big (to change the lenght of the post's excerpt in the blog view)
- krown_excerptmore (to change the chars which appear after the post's excerpt)
- krown_excerpt
- krown_search_form (to modify the structure of the search form)
- krown_pagination
- krown_check_page_title (builds up the title of the page)
- krown_custom_header (outputs custom headers for pages)
- krown_sidebar (to determine the sidebar)
- krown_sidebar_output (to insert the sidebar)
- krown_share_buttons (adds social sharing buttons)

--- Below you have a list of the ones which you can look up in the parent theme's includes/custom-styles.php file and redeclare here:

- krown_custom_css (if you want to get rid of the custom css output in the DOM and move everything here) 

--- Below you have a list of the ones which you can look up in the parent theme's includes/ignitiondeck-functions.php file and redeclare here:

- krown_add_dashboard_links (adds dashboard links when IDE is activated)
- krown_author_profile (builds up the author profile on ID archive pages)
- krown_id_ppp (sets ID projects per page in ID archives)

*/

include( 'includes/plugins.php' ); // Includes the plugins all over again

global $ultimatemember;
$current_user = wp_get_current_user();
if ((isset($user)) and ($user instanceof WP_User)) {
	$current_role = $current_user->roles;
	$current_login = $current_user->data->user_login;
} else {
	$current_role = null;
	$current_login = null;
}
//prar($ultimatemember );
if( is_object( $ultimatemember) && isset( $ultimatemember->user ) ) {
	$um_user = $ultimatemember->user->data;
	$um_login = $um_user['data']['user_login'];
} else {
	$um_user = null;
	$um_login = null;
}


function gradlift_scripts() {
	// load custom javascript file
wp_enqueue_script('gradlift_js', get_stylesheet_directory_uri() . '/js/scripts.js', array( 'jquery' ), null);
// declare javascript variable so ajax can call php functions
wp_localize_script( 'gradlift_js', 'gradlift_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

}

add_action( 'wp_enqueue_scripts', 'gradlift_scripts' ); 


// add ajax call to update user status
add_action('wp_ajax_update_gradlift', 'update_gradlift');

//function to update transcripts
function update_gradlift(){
    global $ultimatemember;
    $user_data = $_POST['user_data'];

    if ($user_data == 'transcripts') {
        $to_update['are_transcripts_submitted'] = 'true';    
    }
    if ($user_data == 'summary') {
        $to_update['is_summary_submitted'] = 'true';    
    }
    if ($user_data == 'loans') {
        $to_update['are_loans_submitted'] = 'true';    
    }
    if ($user_data == 'terms') {
        $to_update['agreed_terms'] = 'true';
    }
    
    $ultimatemember->user->update_profile( $to_update );
    
    $user_id = $ultimatemember->user->id; 
    do_action( 'um_after_user_updated', $user_id );

    exit;
}

// add ajax call to update user status
add_action('wp_ajax_gradlift_update_user_data', 'gradlift_update_user_data');

//function to update transcripts
function gradlift_update_user_data( $user_data = null){
    global $ultimatemember;
    $user_data = $_POST['user_data'];

    if ($user_data == 'transcripts') {
        $to_update['are_transcripts_submitted'] = 'true';    
    }
    if ($user_data == 'summary') {
        $to_update['is_summary_submitted'] = 'true';    
    }
    if ($user_data == 'loans') {
        $to_update['are_loans_submitted'] = 'true';    
    }
    if ($user_data == 'terms') {
        $to_update['agreed_terms'] = 'true';   
    }
    
    $ultimatemember->user->update_profile( $to_update );

    $user_id = $ultimatemember->user->id; 
    do_action( 'um_after_user_updated', $user_id );

    exit;
}


// add gradline tab and change name of "About" tab
add_filter('um_profile_tabs', 'um_profile_tabz', 101 );
function um_profile_tabz( $tabs ) {
    global $ultimatemember;
	//prar($ultimatemember->user->id);
   // $current_user = wp_get_current_user();
	//prar($current_user);
    //$current_role = $current_user->roles;
    //$current_login = $current_user->data->user_login;
    //$current_nicename = $current_user->data->user_nicename; // this is the login username

    //$um_user = $ultimatemember->user->data;
	//prar($um_user);
    //$um_user['data']['user_login'];
    //$um_nicename['data']['user_nicename']; // this is the login username
	if( gradlift_ok_for_project($ultimatemember->user->id) ) {
		$tabs['main'] = array(
			'name' => __('GradLine','ultimatemember'),
			'icon' => 'um-faicon-graduation-cap'
		);

	   // if ( $current_login == $um_login || $current_role[0] == "administrator") {
		if ( um_current_user_can('edit', $ultimatemember->user->id ) ) {
			$tabs['campaign'] = array(
				'name' => __('Your GradLift Scholarship','ultimatemember'),
				'icon' => 'um-faicon-user'
			);
		   }
		//}

		$tabs['comments'] = array(
			'name' => __('Comments','ultimatemember'),
			'icon' => 'um-faicon-comment',
			'count' => max_um_count_user_comments(),
		);
	}
    
    return $tabs;
}

// load custom profile template
add_action('um_profile_content_main', 'load_profile');
function load_profile(){
	//if( !gradlift_project_exists() ) {
		//echo 'project does not exist';
		gradlift_um_new_user_add_project();
	//}
	if( gradlift_ok_for_project() ) {
		include 'templates/_profile.php';
	} else {
		include 'templates/_dashboard.php';
	}
}

// load "your Gradlift Scholarship Page"
add_action('um_profile_content_campaign', 'load_campaign');
function load_campaign() {
    global $ultimatemember;
    /*$current_user = wp_get_current_user();
    $current_role = $current_user->roles;
    $current_login = $current_user->data->user_login;
    $current_nicename = $current_user->data->user_nicename; // this is the login username

    $um_user = $ultimatemember->user->data;
    $um_login = $um_user['data']['user_login'];
    $um_nicename['data']['user_nicename']; // this is the login username

    //if ( $current_login == $um_login || $current_role[0] == "administrator") {*/
	if ( um_current_user_can('edit', $ultimatemember->user->id ) )  include 'templates/_projects.php';
    //}
}



//	remove_action('um_profile_header', 'um_profile_header' );
	add_action('max_um_profile_header', 'max_um_profile_header' );

	function max_um_profile_header( $args ) {
		global $ultimatemember;
		
		$classes = null;
		
		// MAX EDIT		
                //$user_stuff = $ultimatemember->user->usermeta; 
				$user_stuff = $ultimatemember->user->profile; 
				//prar($user_stuff);
                $user_school = $user_stuff['school'];
                $user_degree = $user_stuff['major'];

		if ( !$args['cover_enabled'] ) {
			$classes .= ' no-cover';
		}
		
		$default_size = str_replace( 'px', '', $args['photosize'] );
		
		$overlay = '<span class="um-profile-photo-overlay">
			<span class="um-profile-photo-overlay-s">
				<ins>
					<i class="um-faicon-camera"></i>
				</ins>
			</span>
		</span>';
		
		?>
		
			<div class="um-header<?php echo $classes; ?>">
			
				<?php do_action('um_pre_header_editprofile', $args); ?>
				
				<div class="um-profile-photo" data-user_id="<?php echo um_profile_id(); ?>">

					<a href="<?php echo um_user_profile_url(); ?>" class="um-profile-photo-img" title="<?php echo um_user('display_name'); ?>"><?php echo $overlay . get_avatar( um_user('ID'), $default_size ); ?></a>
					
					<?php
					
					if ( !isset( $ultimatemember->user->cannot_edit ) ) { 
					
						$ultimatemember->fields->add_hidden_field( 'profile_photo' );
						
						if ( !um_profile('profile_photo') ) { // has profile photo
						
							$items = array(
								'<a href="#" class="um-manual-trigger" data-parent=".um-profile-photo" data-child=".um-btn-auto-width">'.__('Upload photo','ultimatemember').'</a>',
								'<a href="#" class="um-dropdown-hide">'.__('Cancel','ultimatemember').'</a>',
							);
							
							echo $ultimatemember->menu->new_ui( 'bc', 'div.um-profile-photo', 'click', $items );
							
						} else if ( $ultimatemember->fields->editing == true ) {
						
							$items = array(
								'<a href="#" class="um-manual-trigger" data-parent=".um-profile-photo" data-child=".um-btn-auto-width">'.__('Change photo','ultimatemember').'</a>',
								'<a href="#" class="um-reset-profile-photo" data-user_id="'.um_profile_id().'" data-default_src="'.um_get_default_avatar_uri().'">'.__('Remove photo','ultimatemember').'</a>',
								'<a href="#" class="um-dropdown-hide">'.__('Cancel','ultimatemember').'</a>',
							);
							
							echo $ultimatemember->menu->new_ui( 'bc', 'div.um-profile-photo', 'click', $items );
							
						}
					
					}
					
					?>
					
				</div>
				
				<div class="um-profile-meta">
				
					<div class="um-main-meta">

						<?php if ( $args['show_name'] ) { ?>
						<div class="um-name">
							
							<a href="<?php echo um_user_profile_url(); ?>" title="<?php echo um_user('display_name'); ?>"><?php echo um_user('display_name'); ?></a>
							
							<?php do_action('um_after_profile_name_inline', $args ); ?>
						
                        <?php 
                            if ($user_school) {
                                echo '<div class="um-school">';
                                echo $user_school;
                                echo '</div>';
                            }
                            if ($user_degree) {
                                echo '<div class="um-degree">';
                                echo $user_degree;
                                echo '</div>';
                            }
                        ?>                   
						</div>
						<?php } ?>
						
						<div class="um-clear"></div>
						
						<?php do_action('um_after_profile_header_name_args', $args ); ?>
						<?php do_action('um_after_profile_header_name'); ?>
						
					</div>
					
					<?php if ( isset( $args['metafields'] ) && !empty( $args['metafields'] ) ) { ?>
					<div class="um-meta">
						
						<?php echo $ultimatemember->profile->show_meta( $args['metafields'] ); ?>
							
					</div>
					<?php } ?>

					<?php if ( $ultimatemember->fields->viewing == true && um_user('description') && $args['show_bio'] ) { ?>
					
					<div class="um-meta-text"><?php echo um_filtered_value('description'); ?></div>
					
					<?php } else if ( $ultimatemember->fields->editing == true  && $args['show_bio'] ) { ?>
					
					<div class="um-meta-text">
						<textarea placeholder="<?php _e('Tell us a bit about yourself...','ultimatemember'); ?>" name="<?php echo 'description-' . $args['form_id']; ?>" id="<?php echo 'description-' . $args['form_id']; ?>"><?php if ( um_user('description') ) { echo um_user('description'); } ?></textarea>
						
						<?php if ( $ultimatemember->fields->is_error('description') )
							echo $ultimatemember->fields->field_error( $ultimatemember->fields->show_error('description') ); ?>
						
					</div>
					
					<?php } ?>
					
					<div class="um-profile-status <?php echo um_user('account_status'); ?>">
						<span><?php printf(__('This user account status is %s','ultimatemember'), um_user('account_status_name') ); ?></span>
					</div>
					
				</div><div class="um-clear"></div>
				
			</div>
			
		<?php
	}


//	remove_action('init', array( 'UM_Builtin', 'set_predefined_fields' ) );
	add_action('max_um_reset_password_form', 'max_um_reset_password_form');
	function max_um_reset_password_form() {
$form = <<<END
		<div class="um-field um-field-password_reset_text" data-key="password_reset_text">
								<div class="um-field-block"><div style="text-align:center">To reset your password, please enter your email address below</div></div>
							</div><div class="um-field um-field-username_b" data-key="username_b"><div class="um-field-area"><input  class="um-form-field valid " type="text" name="username_b" id="username_b" value="" placeholder="Enter your email" data-validate="" data-key="username_b" autocomplete="on" />
							
						</div></div>		
		<div class="um-col-alt um-col-alt-b">
		
			<div class="um-center"><input type="submit" value="Reset my password" class="um-button" /></div>
			
			<div class="um-clear"></div>
			
		</div>
END;
echo $form ;
	}

	add_action('max_um_change_password_form', 'max_um_change_password_form');
	function max_um_change_password_form() {
	
		global $ultimatemember;

		$fields = $ultimatemember->builtin->get_specific_fields('user_password'); ?>
		
		<?php $output = null;
		foreach( $fields as $key => $data ) {
			$output .= $ultimatemember->fields->edit_field( $key, $data );
		}echo $output; ?>
		
		<div class="um-col-alt um-col-alt-b">
		
			<div class="um-center"><input type="submit" value="<?php _e('Change my password','ultimatemember'); ?>" class="um-button" /></div>
			
			<div class="um-clear"></div>
			
		</div>
		
		<?php
		
	}

//add_filter('comment_form_default_fields', 'max_custom_fields');
add_action( 'comment_form_logged_in_after', 'additional_fields' );
add_action( 'comment_form_after_fields', 'additional_fields' );
function additional_fields() {
		global $ultimatemember;
                $user_id = $ultimatemember->user->id; 
		echo  '<input type="hidden" name="max_um_user_id" value="' . $user_id . '" />';
}
function max_custom_fields($fields) {
		// MAX EDIT		
		global $ultimatemember;
                $user_id = $ultimatemember->user->id; 
		$fields['max_um_user_id'] = '<input type="hidden" name="max_um_user_id" value="' . $user_id . '" />';
//		return $fields ;
}
	add_filter('preprocess_comment', 'max_change_comment_author') ;
	function max_change_comment_author( $comment ) {
	//print_r( $comment) ;
	$comment['user_ID'] = (int) $_POST['max_um_user_id'];
	return $comment ;
	}

	// copy from plugins/ultimate-member/core/um-user-posts.php
	function max_um_count_user_comments( $user_id = null ) {
		global $wpdb;
		if ( !$user_id )
			$user_id = um_user('ID');
		
		if ( !$user_id ) return 0;

		$count = $wpdb->get_var("SELECT COUNT(comment_ID) FROM " . $wpdb->comments. " WHERE user_id = " . $user_id . " AND comment_approved = '1'");
		
		return apply_filters('um_pretty_number_formatting', $count);
	}

/****************** PERKSTER SOLUTIONS **********************/	
//Perkster Solution Additions

/**
 *  @brief Prints variable if logged in as administrator
 *  
 *  @param [in] $array The array or variable to be printed
 *  @param [in] $debug Prints a debug backtrace
 *  @param [in] $noAdmin Overrides the admin logged in requirement
 *  @return Will return true if successful
 *  
 *  @details Details
 */
function prar($array = null, $options = false, $noAdmin = false) {
	/*if( is_array( $options ) && isset( $options['user'] ) && wp_get_current_user() ) {
		echo $options['user'] . ' is logged in!';
	}*/
if ( ( is_user_logged_in() && current_user_can( 'manage_options' ) ) || $noAdmin || ( is_array($options) && isset( $options['noAdmin'] ) && $options['userID'] == get_current_user_id() )||  ( is_array($options) && isset( $options['noAdmin'] ) ) ) {
		echo '<pre>';
		if( isset( $options['debug'] ) ) debug_print_backtrace();
		//if( $debug ) debug_print_backtrace();
		if( is_object( $array ) ) {
			print_r ($array);
		} elseif( is_array( $array ) ) {
			print_r( $array );	
		} elseif( !is_null( $array ) ) {
			$string = esc_html( $array );
			echo "<p> $string</p>";
		}
		echo '</pre>';
		return true;
	}
	
}	
  
/*function prar_c($array = null, $debug = null) {
	echo '<!--<pre>';
		if( $debug ) debug_print_backtrace();
		if( is_object( $array ) ) {
			print_r ($array);
		} elseif( is_array( $array ) ) {
			print_r( $array );	
		} elseif( !is_null( $array ) ) {
			$string = esc_html( $array );
			echo "<p> $string</p>";
		}
		echo '</pre>-->';
	}
}*/	


add_action( 'wp_enqueue_scripts', 'remove_default_stylesheet', 200 );

function remove_default_stylesheet() {
    
    wp_dequeue_style( 'krown-style' );
    wp_deregister_style( 'krown-style' );
	
	wp_register_style( 'gradlift-style', get_stylesheet_directory_uri() . '/style.css' , false,filemtime( get_stylesheet_directory() . '/style.css' ), 'all' );
    //wp_register_style( 'gradlift-style', get_stylesheet_directory_uri() . '/style.css', false, '1.0.0' ); 
    wp_enqueue_style( 'gradlift-style' );

}

//Add og:image:secure_url for Ultimate Member
add_action('wp_head', 'add_og_secure_image_in_head');
function add_og_secure_image_in_head(){
 global $post;
 if(!empty($post)){
	global $ultimatemember;
	if( isset ( $ultimatemember ) ) {
		um_fetch_user( um_get_requested_user() );
		if ( um_profile('profile_photo') ) {
			$avatar = um_user_uploads_uri() . um_profile('profile_photo');
		} else {
			$avatar = um_get_default_avatar_uri();
		}
		um_reset_user(); 
		echo '<meta property="og:image:secure_url" content="' . $avatar . '" />';
		remove_action('wp_head', 'rel_canonical');
	}
    
  }//end of if
 }//end of function

function check_if_um( $um = null ) {
	global $ultimatemember;
	if( isset ( $ultimatemember ) ) {
		$um = true;
	}
	return $um;
}

add_filter( 'wpseo_canonical', 'wpseo_canonical_exclude' );
function wpseo_canonical_exclude( $canonical ) {
	if( check_if_um() ) $canonical = false;
	return $canonical;
}
	
add_filter( 'wpseo_description', 'wpseo_description_exclude' );
function wpseo_description_exclude( $description ) {
	if( check_if_um() ) $description = false;
	return $description;
}

//Add code for accepting terms
add_action( 'wp_ajax_nopriv_gradlift_accept_terms', 'gradlift_accept_terms' );
add_action( 'wp_ajax_gradlift_accept_terms', 'gradlift_accept_terms' );

function gradlift_accept_terms() {
	global $ultimatemember;
	$user_id = $ultimatemember->user->id;
		
	$success = update_usermeta($user_id, 'agreed_terms', 'true');
	$ultimatemember->user->update_profile( array('agreed_terms', true));
	update_user_meta($user_id, 'test_key', 'true');
	$ultimatemember->user->update_profile( array('agreed_terms', 'true'));
	
	error_log("-----Right Here-----");
	error_log($success);
	error_log(print_r($ultimatemember->user, true));
	error_log(print_r($ultimatemember, true));
	echo $user_id;
	return $success;
}

//Include custom functions for integration between ignition deck and ultimate member.
include( 'includes/ultimate-member-ignition-deck.php' ); 
//prar( $current_user );
add_action( 'um_after_profile_name_inline', 'gradlift_um_after_profile_name_inline' );

function gradlift_um_after_profile_name_inline( $args = null ) {
	//prar( 'after profile' );
}
/*---------------------------------
	Custom header
------------------------------------*/

if ( ! function_exists( 'gradlift_custom_header' ) ) {

	function gradlift_custom_header() {

		global $post;

		$output = '';

		if ( isset( $post ) ) {

			$header_type = get_post_meta( $post->ID, 'krown_custom_header_set', true );

			if ( ! is_search() ) {

				if ( is_page_template( 'template-slider.php' ) && ( $header_type == 'w-custom-header-image' || $header_type == 'w-custom-header-slider' || $header_type == 'w-custom-header-html' ) ) {

					// Get values

					$header_height = get_post_meta( $post->ID, 'krown_custom_header_height', true );

					// Configure header object based on type

					$output .= '<div id="custom-header" class="header-below"' . ( $header_type != 'w-custom-header-slider' ? ' style="height:' . $header_height . 'px"' : '' ) . '>';

					if ( $header_type == 'w-custom-header-image' ) {
						$output .= '<div class="header-image" style="background-image:url(' . get_post_meta( $post->ID, 'krown_custom_header_img', true ) . '); height:' . $header_height . 'px"></div>';

					} else if ( $header_type == 'w-custom-header-slider' ) {
						$output .= do_shortcode( get_post_meta( $post->ID, 'krown_custom_header_slider', true ) );
					} else if ( $header_type == 'w-custom-header-html' ) {

						$ctas = get_post_meta( $post->ID, 'krown_custom_header_cta', true );

						if ( ! empty( $ctas ) ) {

							$ctawidth = 100 / sizeof( $ctas );

							foreach ( $ctas as $cta ) {

								$output .= '<div class="cta" style="width:' . $ctawidth . '%"><div class="cta-back" style="background-image:url(' . $cta['image'] . ')"></div>' . '<div class="cta-holder"><h2>' . $cta['text'] . '</h2><div class="cta-link"><a class="krown-button custom" href="' . $cta['b_link'] . '" target="' . $cta['b_target'] . '">' . $cta['b_label'] . '</a></div></div></div>';

							} 

						}

					}

					$output .= '</div>';

					// Add custom tabs if present

					$custom_tabs = get_post_meta( $post->ID, 'krown_custom_header_slider_complex', true );

					if ( ! empty( $custom_tabs ) && $header_type == 'w-custom-header-slider' ) {

						preg_match( "/(revapi.+;)/", $output, $matches );
						if ( ! empty( $matches[0] ) ) {
							$api = str_replace( array( ';', "\n" ), '', $matches[0] );
						}

						$output .= '<div id="custom-tabs"><ul class="wrapper clearfix">';
						$size_class = ' style="width:' . ( 100 / sizeof( $custom_tabs ) ) . '%"';

						foreach ( $custom_tabs as $tab ) {
							$output .= '<li' . $size_class . '><h4>' . $tab['title'] . '</h4>';
							if ( $tab['icon'] != 'icon-none' ) {
								$output .= '<i class="krown-' . $tab['icon'] . '"></i>';
							}
							$output .= '</li>';
						}

						$output .= '</ul></div>';

						$output .= '<script type="text/javascript">
							var $ = jQuery.noConflict();
							$(document).ready(function(){

								var $revTabs = $("#custom-tabs").find("li");

								' . $api . '.on("revolution.slide.onchange", function(e, data){
									$revTabs.removeClass("selected");
									$revTabs.eq(data.slideIndex-1).addClass("selected");
								});

								$revTabs.click(function(){
									' . $api . '.revshowslide($(this).index()+1);
								});

							});
						</script>';

					}

				}

			}

		}

		echo $output;

	}

}
?>