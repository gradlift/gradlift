<?php


use \Dropbox as dbx;



function boxit_install() {

	/***************************************************/
	/*                                                 */
	/*       STEP 1                    	               */
	/*                                                 */
	/*       Create config file                        */
	/*                                                 */
	/***************************************************/

	/**
	 * SAVE ACTION AFTER SUBMITINGSTEP 1
	 */
	if(isset($_POST['boxit_key']) && isset($_POST['boxit_secret'])) {
		/*if(!file_exists(dirname(__FILE__)."/app-info.json")) {
		    $content = '{"key": "'.$_POST['boxit_key'].'","secret": "'.$_POST['boxit_secret'].'"}';
			$fp = fopen(dirname(__FILE__)."/app-info.json","wb");
			fwrite($fp,$content);
			fclose($fp);
		}*/
		$json = '{"key": "'.$_POST['boxit_key'].'","secret": "'.$_POST['boxit_secret'].'"}';
		update_option('dropbox_key_secret', $json);
		config_file();
	}

	/**
	 * INSTALATION - STEP 1
	 */
	//if(!file_exists(dirname(__FILE__)."/app-info.json")) {
	if(!get_option('dropbox_key_secret')) {
		//Check PHP version
		$php_version = explode('.', phpversion());
		if((int)$php_version[1] < 3) :
		    ?>
		    <div style="margin-left:0px; margin-top:20px;" class="error">
		        <p>Error: BOXIT requires PHP version  >= 5.3<br>You are runing PHP <?php echo phpversion() ?><br>Contact your hosting provider and ask for PHP upgrade.</p>
		    </div>
		    <?php
		    die();
		endif;
		//checking support for 64 bit integers - required by Dropbox API (made silent in dropbox-sdk/Dropbox/RequestUtil.php#22)
        if (strlen((string) PHP_INT_MAX) < 19) : ?>
            <div style="margin-left:0px; margin-top:20px; border-left:4px solid #FFCD28;" class="error">
                <p>Warning: You are using the 32-bit build of PHP (Dropbox SDK uses 64-bit integers). This could cause problems because some of the numbers used by Dropbox API (file sizes, quota, etc) can be larger than 32-bit ints can handle.</p>
            </div>
        <?php endif ?>
		<form action="" method="POST" class="boxit_create_json">
			<h3>BOXIT installation - step 1 of 3</h3>
			<h4>Create the configuration file required by Dropbox API.</h4>
			<p>Follow <a href="https://www.dropbox.com/developers/apps" target="_blank" >THIS</a> link and create Dropbox application that will be communicating with BOXIT</p>
			<label for="boxit_key"><span style="width:120px; display:inline-block">Your app key:</span><input type="text" name="boxit_key" id="boxit_key"></label>
			<br><br>
			<label for="boxit_secret"><span style="width:120px; display:inline-block">Your app secret:</span><input type="text" name="boxit_secret" id="boxit_secret"></label>
			<br><br>
			<input type="submit" class="button" value="Create config file">
		</form>
		<?php
		return false;
	}



	$appInfo = dbx\AppInfo::loadFromJsonFile(dirname(__FILE__)."/app-info.json");
	$webAuth = new dbx\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");


	/***************************************************/
	/*                                                 */
	/*       STEP 2                    	               */
	/*                                                 */
	/*       Save authorization code &                 */
	/*       genereate the access token                */
	/*                                                 */
	/***************************************************/

	/**
	 * SAVE ACTION AFTER SUBMITINGSTEP 2
	 */
	if(isset($_POST['auth_code'])) {

		//save the authorization code
		delete_option('dropbox_auth_code');
		add_option( 'dropbox_auth_code', $_POST['auth_code']);

		//convert the authorization code into an access token and save it
		try {
		    $finish = $webAuth->finish($_POST['auth_code']);
		} catch (Exception $e) {
		    echo '<br><div style="margin-left:0px;" class="error"><p>Caught exception: '.$e->getMessage().'</p></div>';
		    reinstall_boxit_link();
		}
		if($finish != NULL) {
			list($accessToken, $dropboxUserId) = $finish;
			delete_option('dropbox_access_token');
			add_option('dropbox_access_token', $accessToken);
		}

	}

	//var_dump(get_option('boxit_auth_code'));
	//var_dump(get_option('access_token'));

	/**
	 * INSTALATION - STEP 2
	 */
	if(!get_option('dropbox_auth_code') || !get_option('dropbox_access_token')) {
		$authorizeUrl = $webAuth->start();
		?>
		<form action="" method="POST" class="boxit_authorize">
			<h3>BOXIT installation - step 2 of 3</h3>
			<h4>Allow your Dropbox App to connect to your Dropbox account.</h4>
			<p>Go to the URL below, click ALLOW and copy the authorization code (you have to log in to dropbox first)</p>
			<p>Authorization URL: <a target="_blkank" href="<?php echo $authorizeUrl ?>"><?php echo $authorizeUrl ?></a></p>
			<label for="boxit_authorization_code">Paste the authorization code here:<input style="margin-left:20px; width:400px;" type="text" name="auth_code" id="boxit_authorization_code"></label>
			<br><br>
			<input type="submit" class="button" value="Generate the access token">
		</form>
		<?php
		return false;
	}




	/**
	 * TESTING THE CONNECTION
	 */
	?>
	<h3>BOXIT installation - step 3 of 3</h3>
	<h4>Checking the API connection.</h4>
	<?php
	if(get_option('dropbox_access_token')) {
		$dbxClient = new dbx\Client(get_option('dropbox_access_token'), "PHP-Example/1.0");
		try {
		    $accountInfo = $dbxClient->getAccountInfo();
		} catch (Exception $e) {
		    echo '<div style="margin-left:0px;" class="error"><p>Caught exception: '.$e->getMessage().'</p></div>';
		    reinstall_boxit_link();
		}
		if($accountInfo !== NULL) {
			echo '<div style="margin-left:0px;" class="updated"><p>Installation complete. Connection with Dropbox API seems to be OK.</p></div>';
			echo '<br><a href="'.admin_url('options-general.php?page=boxit_settings').'" class="button">Go to BOXIT settings</a>';
		}
	}
}




function reinstall_boxit_link() {
	echo '<br><a href="'.admin_url('options-general.php?page=boxit_settings&reinstall_boxit').'" class="button">Run instalation again.</a>';
}












